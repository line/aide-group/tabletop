{ # Ref: https://gyp.gsrc.io/index.md
  "targets": [{
   "target_name": "wrapper",
    "sources": [ 
      "./node_modules/aideweb/src/WrapperCCService.cpp",
      "./src/wrap.cpp"
    ],
    "cflags_cc": [ "-Wno-psabi", "-fexceptions", "-Wno-varargs" ],
    "cflags_cc!": [ "-fno-exceptions", "-fno-rtti", "-Wvarargs" ],
    "defines": [ "ON_GYP", "NAPI_DISABLE_CPP_EXCEPTIONS" ],
    "include_dirs": [
      "./src",
      "./node_modules/aideweb/src",
      "/usr/include/python3.10",
      "/usr/include/python3.7m",
      "<!@(node -p \"require('node-addon-api').include\")"
    ],
    "library_dirs": [
     "../node_modules/.lib"
    ],
    "libraries": [
      "-lcurl",
      "-laidesys",
      "-lwjson",
      "-ltabletop",
      "-lopencv_videoio",
      "-lopencv_xphoto",
      "-lopencv_bgsegm",
      "-lopencv_video",
      "-lopencv_highgui",
      "-lopencv_imgproc",
      "-lopencv_imgcodecs",
      "-lopencv_core"
    ],
    "conditions": [
      [ '"<!(uname -m)" == "armv7l"', # RPi specific config  
        {
          "libraries": [ 
	    "-lraspicam_cv",
            "-lpython3.7m" 
          ]   
        },
        {
          "libraries": [ 
            "-lpython3.13" 
          ]   
        }
      ]
    ]
  }]
}

