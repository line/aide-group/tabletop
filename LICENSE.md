![AIDE logo](./etc/www/img/aide-logo.png) 

# AIDE LICENSE AND COPYRIGHT

AIDE is free software and resource, original files are licensed under:

* [CeCILL-C](https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) for the code.
* [CC-BY](https://creativecommons.org/licenses/by/3.0/fr/) for the contents.

Commercial and non-commercial use are thus permitted in compliance with these licenses, for these files, whereas third-party elements must be used under their related licenses, namely:

* The [Mathieu Manrique](mathieumanrique@gmail.com) The graphic production available in [this folder](https://gitlab.inria.fr/line/aide-group/aide/-/tree/master/%40lola-denet/images/mathieu) is available under the [CC-BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0) license, concretely: 
  * The production can be freely used for non-commercial purpose, as a component of the AIDE bundle.
  * Any other re-use are not allowed, unless a specific agreement with the author is issued.

