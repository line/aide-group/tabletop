
class Instruction {
    constructor(image, x, y, width, height, instruction, value) {
      this.image = image;
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.instruction = instruction;
      this.value = value;
    }
  
    draw() {
      ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
  }
  
  const addInstruction = function (instruction, value, velocityID = "") {
    let image;
    if (instruction == 'forward') {
      value = document.getElementById(velocityID).value;
      if (value == 1) {
        image = "forward1";
      } else if (value == 2) {
        image = "forward2";
      } else if (value == 3) {
        image = "forward3";
      } else {
        alert("Instruction incorrecte");
      }
    } else if (instruction == "turn") {
      if (value == -90) {
        image = "turnLeft";
      } else if (value == 90) {
        image = "turnRight";
      } else {
        alert("Instruction incorrecte");
      }
    } else if (instruction == "start") {
      image = "startMovement";
    }
    if (instructions.length <= table.length) {
      let location = table[instructions.length];
      if (instruction == 'start') {
        location = table[table.length - 1];
      }
      instructions[instructions.length] = new Instruction(document.getElementById(image), location.x, location.y, location.width, location.height, instruction, value);
      instructions[instructions.length - 1].draw();
      if (instructions[instructions.length - 1].instruction == 'turn') {
        angle = instructions[instructions.length - 1].value;
        velocity = 0;
      } else if (instructions[instructions.length - 1].instruction == 'forward') {
        angle = 0;
        velocity = instructions[instructions.length - 1].value;
      }
      moveBot(angle, velocity);
    } else {
      alert("Maximum 9 instructions ('start' inclus)");
      initCanvas();
    }
  }
  
  