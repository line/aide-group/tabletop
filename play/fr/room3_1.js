/**
 * - Initializes the global variables specific to the operation of the canvas for the room concerned.
 * @file 
 */


/** Initialization of the canvas.
 * - Create the canvas. 
 * @memberof Variables
 * @ignore
 * @const {HTMLCanvasElement} canvas
 */
const canvas = document.querySelector("canvas");

/** Initialization of the context.
 * - Create the canvas context. 
 * @memberof Variables
 * @ignore
 * @const {CanvasRenderingContext2D} ctx 
 */
const ctx = canvas.getContext("2d");

// Creation of room-specific global variables.
var backgroundID = 'canvasBackground';
var canvasHiddenData = {gameButtonID: 'startGame'};
var free = { audioID: 'audioOzon', textID : 'ozonText', audioSource: './audio/Room3_1FreeAudioSource.mp3', textFree : 'ozonTextRoom3_1Free', audioReplay : './audio/Room3_1FreeAudioReplay.mp3', textReplay1 : 'ozonTextRoom3_1Replay1',  audioReplay2:'./audio/Room3_1FreeAudioReplay2.mp3', textReplay2 : 'ozonTextRoom3_1Replay2',  buttonID: 'continueButton'}; 
var x0 = canvas.width * 0.33, y0 = canvas.height * 0.764, xCoef = x0 * 0.192, yCoef = y0 * 0.102, xGrid=new Array(5), yGrid=new Array(5), numberGrid = new Array(25), replayed =0;