/**
 * - Initializes the global variables specific to the operation of the canvas for the room concerned.
 * @file 
 */


/** Initialization of the canvas.
 * - Create the canvas. 
 * @memberof Variables
 * @ignore
 * @const {HTMLCanvasElement} canvas
 */
const canvas = document.querySelector("canvas");

/** Initialization of the context.
 * - Create the canvas context. 
 * @memberof Variables
 * @ignore
 * @const {CanvasRenderingContext2D} ctx 
 */
const ctx = canvas.getContext("2d");

// Creation of room-specific global variables.
var backgroundID = 'canvasBackground';
var canvasHiddenData = {gameButtonID: 'startGame'};
var free = { audioID: 'audioOzon', textID: 'ozonText', audioSource: './audio/Room2_1FreeAudioSource.mp3', textFree1:"ozonTextRoom2_1Free1",  audioSource2:'./audio/Room2_1FreeAudioSource2.mp3', textFree: "ozonTextRoom2_1Free2", audioReplay : './audio/Room2_1FreeAudioReplay.mp3', textReplay: "ozonTextRoom2_1Replay", buttonID: 'continueButton', gobID : 'Gob', gobOnclick : 'Gob2'}; 
var whiteBoard={}, replayed =0;
