/**
 * - Manages the operation of the simulator interface.
 * @file 
 */

 /**
  * - In the simulator.js file. 
  * - Manages the display and operation of the simulator.
  * @namespace Simulator
  */
//// Classes ////

/** Class in charge of interactions with the robot.
 * - Contains the robot's own constructor and the functions that allow it to manage its display and movements.
 * @memberof Simulator
 */
class Ozobot {

  /** Constructor
   * - Defines the architecture of the Ozobot object.
   * @param {string} image The Ozobot image ID.
   * @param {double} x The x coordinate of Ozobot.
   * @param {double} y The y coordinate of Ozobot.
   * @param {double} xVelocity The velocity of Ozobot's horizontal movement.
   * @param {double} yVelocity The velocity of Ozobot's vertical movement.
   * @param {int} angle The Ozobot orientation.
   * @param {double} width The Ozobot width.
   * @param {double} height The Ozobot height.
   */
  constructor(image, x, y, xVelocity, yVelocity, angle, width, height) {
    this.image = image;
    this.x = x;
    this.y = y;
    this.xVelocity = xVelocity;
    this.yVelocity = yVelocity;
    this.angle = angle;
    this.width = width;
    this.height = height;
  }
  /** Draw the Ozobot on the canvas.
   * - Updates the robot's display on the canvas.
   * - Allows image rotation if needed.
   */
  draw() {
    ctx.save();
    ctx.translate(this.x + (this.width / 2), this.y + (this.height / 2));
    ctx.rotate(this.angle * (Math.PI / 180));
    ctx.translate(-(this.x + (this.width / 2)), -(this.y + (this.height / 2)));
    ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    ctx.restore();
  }
  /** Calculate the new angle based on the desired rotation.
   * - The angle can be positive or negative.
   * @param {int} angle The angle value.
   */
  rotate(angle) {
    this.angle += angle;
  }

  /** Defines the movement velocity. 
   * - Calculates the orientation of the displacement based on the value of the angle.
   * - Defines the x and y coefficients which allows to advance a fixed number of squares.
   * - Check that the displacement does not go beyond the frame.
   * - Prevents displacement if it goes out of frame.
   * @param {int} velocity The number of movement squares.
   */
  setVelocity(velocity) {
    let angle = this.angle % 360;
    let xVel = 1.9, yVel = 1.65;
    if (angle == 90 || angle == -270) { // to the top
      xVel = 0;
      yVel = (-yVel * velocity) * this.height;
    } else if (angle == 270 || angle == -90) { // to the bottom
      xVel = 0;
      yVel = (yVel * velocity) * this.height;
    } else if (angle == 0 || Math.abs(angle) == 360) { // to the left
      xVel = (-xVel * velocity) * this.width;
      yVel = 0;
    } else if (Math.abs(angle) == 180) { // to the right
      xVel = (xVel * velocity) * this.width;
      yVel = 0;
    }
    let testX = this.x + xVel, testY = this.y + yVel;
    if (testX < frame.x || testX > (frame.x + frame.width) || testY < frame.y || testY > frame.height) {
      let notification = new Notification("Déplacement interdit");
      this.xVelocity = 0;
      this.yVelocity = 0;
    } else {
      this.xVelocity = xVel;
      this.yVelocity = yVel;
    }
  }
  
  /** Defines the new Ozobot coordinates.
   * - Calculates the new coordinates of Ozobot according to the velocity of the movement.
   */
  move() {
    this.x += this.xVelocity;
    this.y += this.yVelocity;
  }

  /** Check if the Ozobot encounters an obstacle in his path.
   * - Test if the Ozobot has fallen into the trap. Display a message if it does and resets the simulator.
   * - Test if the Ozobot meets a wall. Return it to its original position if it does.
   * - Check if the Ozobot is under the secret passage. Makes it invisible if it is.
   * - Return a boolean to continue or stop the movement.
   * @param {double} xMovement Horizontal distance traveled to the obstacle.
   * @param {double} yMovement Vertical distance traveled to the obstacle.
   */
  checkPosition(xMovement = 0, yMovement = 0) {
    // Trap
    if (this.x > trap.x && this.x < (trap.x + trap.width) && this.y < (trap.y + trap.height) && this.y > trap.y) {
      setTimeout(function () {
        let notification = new Notification(" Aaaahhhh... Splash ! ");
        initBarriers();
        initCanvas();
        initOzobot();
      }, 900);
      return false;
    }
    // Obstacles
    for (let el of barriers) {
      if (this.x >= el.x && this.x <= (el.x + el.width) && this.y <= (el.y + el.height) && this.y >= el.y || this.x >= el.x && this.x <= (el.x + el.width) && (this.y + this.height) <= (el.y + el.height) && (this.y + this.height) >= el.y) {
        let notification = new Notification("Bam ! Ouch !");
        this.x -= xMovement;
        this.y -= yMovement;
        return false;
      }
    }
    // Secret path
    if (this.x > secretPath.x && this.x < (secretPath.x + secretPath.width) && this.y < (secretPath.y + secretPath.height) && this.y > secretPath.y) {
      this.image = document.getElementById(ozobotInitData['transparentOzobot']);
    } else {
      this.image = document.getElementById(ozobotInitData['ozobotImage']);
    }
    return true;
  }
  /** Check if the robot can leave the room.
   * - Check to see if the Ozobot has found the key. Display a message and open the door if so.
   * - Check to see if the Ozobot is in front of the exit door. Displays a message if so.
   * - Quit training if the Ozobot is in front of the exit and the door is open.
   */
  checkExit() {
    // Key
    if (this.x > key.x && this.x < (key.x + key.width) && this.y > key.y && this.y < (key.y + key.height)) {
      exitDoor.x = canvas.width * 0.87;
      exitDoor.width = canvas.width * 0.13;
      exitDoor.height = canvas.height * 0.03;
      initCanvas();
      this.draw();
      for (let el of instructions) {
        el.draw();
      }
      let notification = new Notification("Clic-clac");
      unlocked = true;
    }
    // Exit 
    if (this.x > exit.x && this.x < (exit.x + exit.width) && this.y > exit.y && this.y < (exit.y + exit.height)) {
      if (unlocked == true) {
        setTimeout(function () {
          alert("Bravo ! Tu as terminé l'entrainement");
          window.location.reload();
        }, 100);
      } else {
        let notification = new Notification("Oh non ! La porte est vérouillée.");
      }
    }
  }
}

/** Class responsible for creating obstacles.
 * - Contains the obstacle constructor and the functions that allow it to manage its display.
 * @memberof Simulator
 */
class Barrier {
  /** Constructor
   * - Defines the architecture of objects representing obstacles.
   * @param {double} x The x coordinate of the obstacle.
   * @param {double} y The x coordinate of the obstacle.
   * @param {double} width The obstacle width.
   * @param {double} height The obstacle height. 
   */
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  /** Draw the obstacle on the canvas. 
   * - Used to check that the coordinates and size of the obstacle are correct in relation to the canvas.
   */
  draw() {
    ctx.strokeRect(this.x, this.y, this.width, this.height);
    ctx.fillStyle = "rgb(200, 173, 127)"
    ctx.fillRect(this.x, this.y, this.width, this.height);
  }

}

//// Canvas ////

/** Draw and display the initial version of the canvas.
 * - Hides certain elements of the html page.
 * - Draw and display the canvas with a background image and the defined parameters (position, size).
 * @memberof Simulator
 */
const initCanvas = function () {
  document.getElementById(canvasHiddenData['trainingButtonID']).style.display = "none";
  let background = document.getElementById(backgroundID);
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
  canvas.style.display = "block";
  doors[closedDoor].draw();
  exitDoor.draw();
}

/** Create the objects responsible for representing the obstacles.
 * - Stores objects in a array.
 * - Randomly determine which door to the secret passage will be opened or closed.
 * @memberof Simulator
 */
const initBarriers = function () {
  frame = new Barrier(canvas.width * 0.3, 0, canvas.width * 0.7, canvas.height * 0.9);
  trap = new Barrier(canvas.width * 0.66, canvas.height * 0.45, canvas.width * 0.15, canvas.height * 0.2);
  secretPath = new Barrier(canvas.width * 0.3, 0, canvas.width * 0.34, canvas.height * 0.46);
  topTrapWall = new Barrier(canvas.width * 0.65, canvas.height * 0.42, canvas.width * 0.18, canvas.height * 0.05);
  rightTrapWall = new Barrier(canvas.width * 0.82, canvas.height * 0.43, canvas.width * 0.02, canvas.height * 0.23);
  bottomTrapWall = new Barrier(canvas.width * 0.65, canvas.height * 0.66, canvas.width * 0.18, canvas.height * 0.05);
  oneWall = new Barrier(canvas.width * 0.66, canvas.height * 0.18, canvas.width * 0.18, canvas.height * 0.05);
  key = new Barrier(canvas.width * 0.48, canvas.height * 0.67, canvas.width * 0.17, canvas.height * 0.22);
  doorA = new Barrier(canvas.width * 0.28, canvas.height * 0.45, canvas.width * 0.2, canvas.height * 0.03);
  doorB = new Barrier(canvas.width * 0.48, canvas.height * 0.45, canvas.width * 0.18, canvas.height * 0.03);
  exitDoor = new Barrier(canvas.width * 0.98, canvas.height * 0.005, canvas.width * 0.018, canvas.height * 0.18);
  exit = new Barrier(canvas.width * 0.84, canvas.height * 0.005, canvas.width * 0.15, canvas.height * 0.18);
  doors = [doorA, doorB];
  closedDoor = getRandomInt(doors.length);
  barriers = [topTrapWall, rightTrapWall, bottomTrapWall, oneWall, doors[closedDoor]];
}

/** Randomly generate an integer.
 * - Randomly generate an integer in the range [0; max[.
 * @param {int} max Maximum value excluded from the random number generation interval.
 * @memberof Simulator
 */
const getRandomInt = function (max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/** Creates the Ozobot object and initializes its display.
 * - Define the characteristics of the Ozobot and display it on the canvas.
 * - Displays the simulator buttons.
 * @memberof Simulator
 */
const initOzobot = function () {
  ozobot = new Ozobot(document.getElementById(ozobotInitData['ozobotImage']), ozobotInitData['x'], ozobotInitData['y'], ozobotInitData['xVelocity'], ozobotInitData['yVelocity'], ozobotInitData['angle'], ozobotInitData['width'], ozobotInitData['height']);
  ozobot.draw();
  document.getElementById(canvasHiddenData['simulatorID']).style.display = "block";
}

/** Move the Ozobot on the canvas.
 * - Calls the functions that allow you to turn the Ozobot, calculate the displacement and animate the Ozobot on the canvas.
 * @param {int} angle The angle value.
 * @param {int} velocity The number of movement squares.
 * @memberof Simulator
 */
const moveOzobot = function (angle, velocity = 0) {
  ozobot.rotate(angle);
  ozobot.setVelocity(velocity);
  animate();
}

/** Manage the animation of the Ozobot on the canvas.
 * - Use a timer to move the Ozobot around the canvas so that its movements are dragged.
 * - Use two different timers for horizontal and vertical movement.
 * - Calls the functions responsible for checking whether movements are possible.
 * - The timer ends when the movement is finished or when the Ozobot encounters an obstacle.
 * - Hides the simulator buttons when a move is in progress and displays them again when the move is complete in order to reduce errors.
 * @memberof Simulator
 */
const animate = function () {
  let step = -0.5;
  if (ozobot.xVelocity > 0 || ozobot.yVelocity > 0) {
    step = Math.abs(step);
  }
  let i = 0;
  if (ozobot.xVelocity != 0) {
    let timer = setInterval(function () {
      document.getElementById(canvasHiddenData['simulatorID']).style.display = "none";
      ozobot.x += step;
      let goodPosition = ozobot.checkPosition(i, 0);
      initCanvas();
      for (let el of instructions) {
        el.draw();
      }
      ozobot.draw();
      i += step;
      if (ozobot.xVelocity < 0 && i < ozobot.xVelocity || ozobot.xVelocity > 0 && i > ozobot.xVelocity || !goodPosition) {
        clearInterval(timer);
        ozobot.checkExit();
        document.getElementById(canvasHiddenData['simulatorID']).style.display = "block";
      }
    }, 1);
  } else if (ozobot.yVelocity != 0) {
    let timer = setInterval(function () {
      document.getElementById(canvasHiddenData['simulatorID']).style.display = "none";
      ozobot.y += step;
      let goodPosition = ozobot.checkPosition(0, i);
      initCanvas();
      for (let el of instructions) {
        el.draw();
      }
      ozobot.draw();
      i += step;
      if (ozobot.yVelocity < 0 && i < ozobot.yVelocity || ozobot.yVelocity > 0 && i > ozobot.yVelocity || !goodPosition) {
        clearInterval(timer);
        ozobot.checkExit();
        document.getElementById(canvasHiddenData['simulatorID']).style.display = "block";
      }
    }, 1);
  } else {
    ozobot.move();
    ozobot.checkPosition();
    initCanvas();
    for (let el of instructions) {
      el.draw();
    }
    ozobot.draw();
  }
}
