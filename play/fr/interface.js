/**
 * - Manages the operation of the Tabletop interface.
 * @file 
 */

/**
 * - In the interface.js file.
 * - Manages the display and sound of the interface.
 * @namespace Interface 
 */

/**
 * - In the interface.js file.
 * - Manage the form display and form fields.
 * @namespace Form
 */

/**
 * - In the interface.js file.
 * - Manages the operation of the canvas.
 * @namespace Canvas
 */

 /**
  * - In the interface.js file.
  * - Get the Tabletop events and data. 
  * @namespace Tabletop
  */

/** Defines the background web page.
  * - Adapts the background size to the size of the browser window.
  * @memberof Interface
  * @param {string} id The body id.
  * @param {string} image The image adress.
  */
const setBackground = function (id, image) {
  let style = document.getElementById(id).style;
  style.backgroundImage = "url('" + image + "')"
  style.backgroundSize = window.innerWidth + "px " + window.innerHeight + "px";
  style.backgroundRepeat = "no-repeat";
}

/** Creates a slide form and manage the display of divisions.
 * - Hide the displayed division and replace it with the next division.
 * - Defines the division style like a slide form.
 * - Disable clickable image.
 * @memberof Interface
 * @param {string} nextDivID The next division ID (the division to display).
 * @param {string} prevDivID The division ID that will become the previous one (the division to hide).
 */
const showDiv = function (nextDivID, prevDivID) {
  let divStyle = document.getElementById(nextDivID).style;
  if (prevDivID != null) {
    document.getElementById(prevDivID).style.display = "none";
  }
  divStyle.display = "block";
  divStyle.width = (window.innerWidth * 0.7) + "px ";
  divStyle.height = (window.innerHeight * 0.7) + "px ";
  if (document.getElementById("character")) {
    document.getElementById("character").onclick = "null";
  }
}

/** Manages "techno division" display.
 * - Allows the conditional display of the "techno division" according to the age entered.
 * - Sets the input to "required" status.
 * - Handles the case where the "techno division" is the previous one or that where it is the next.
 * - Note: To reinstate this function, you must remove the "required" parameter in the divTechnoOlder, uncomment lines 193 to 198 of the file room0_1.html and replace the function calls on lines 80 and 141 of the same file.
 * @memberof Form
 * @param {string} divID The actual division ID.
 */
const showDivTechno = function (divID) {
  let age = document.getElementsByName("age")[0].value, kids = ["8-10", "11-13"], nextDivID = "";
  if (divID == "divLevel") {
    if (kids.includes(age)) {
      nextDivID = "divTechnoKids";
      document.getElementsByName("technoKids")[0].required = "required";
    } else {
      nextDivID = "divTechnoOlder";
      document.getElementsByName("technoOlder")[0].required = "required";
    }
  } else if (divID = "divProgramming") {
    if (kids.includes(age)) {
      nextDivID = "divTechnoKids";
    } else {
      nextDivID = "divTechnoOlder";
    }
  }
  showDiv(nextDivID, divID);
}

/** Check that the current field is filled before displaying the next question.
 * - Check if at least one checkbox is checked.
 * - Tests if the retrieved value is incorrect and / or missing.
 * - Displays an error message to the user to complete or restart their entry.
 * - If the value entered is present and correct, the callback function is called to go to the next question.
 * @memberof Form
 * @param {string} inputName The name of the input to check.
 * @param {callback} callback The function to execute when the verification is performed (here, the one that manages the display of divisions).
 */
const checkRequiredFields = function (inputName, callback = function () { }) {
  let input = document.getElementsByName(inputName);
  if (input[0].type == "checkbox") {
    let oneChecked = false;
    for (let i = 0; i < input.length && !oneChecked; i++) {
      if (input[i].checked) {
        oneChecked = true;
        callback();
      }
    }
    if (!oneChecked) {
      window.alert("Missing value on input : " + inputName + ". \nAt least one box must be checked. ");
      input[0].focus();
    }
  } else {
    if (input[0].validity.valueMissing) {
      window.alert("Missing value on input : " + inputName);
      input[0].focus();
    } else if (!input[0].validity.valid) {
      window.alert("Incorrect value on input : " + inputName);
      input[0].focus();
    } else {
      callback();
    }
  }
}

/** Replay the character's voice.
 * - Play the audio of the character clicked.
 * - Here we implement a mechanism to overpass the "play() failed because the user didn't interact with the document first" limitation.
 * @memberof Interface
 * @param {string} audioID Audio ID of the character clicked.
 */
const replay = function (audioID) {
  let audio = document.getElementById(audioID);
  let loop = setInterval(() => {
    if (interacting) {
      audio.play();
      clearInterval(loop);
    } else {
      if (alerting) {
        document.getElementById("divMain").innerHTML = "<b>Clique sur ma page !</b>" + document.getElementById("divMain").innerHTML;
	alerting = false;
      }
    }
  }, 1000);
}

let interacting = false, alerting = true;

["click"].forEach((eventName) => {
  window.addEventListener(eventName, (event) => {
    if (!interacting)
      console.log(event);
    interacting = true;
  })
});

/** Mute or unmute the character's voice.
 * - Check if the audio is muted.
 * - Mute or unmute the audio depending the current state. 
 * - Changes the button image according to the current audio state.
 * @memberof Interface
 * @param {string} audioID Audio ID of the current character 
 */
const mute = function (audioID) {
  let audio = document.getElementById(audioID);
  if (audio.muted) {
    audio.muted = false;
    document.getElementById("audio").src = "../images/mathieu/audio_button.png";
  } else {
    audio.muted = true;
    document.getElementById("audio").src = "../images/mathieu/mute_button.png";
  }
}


/** Check the state of the Tabletop.
 * - Returns true if the state of Tabletop has changed.
 * @memberof Tabletop
 */
const hasTabletopChanged = function () {
  // In the real implementation there will be a client request which will return the true data
  return true;
}

/** Check the position of the Tabletop elements.
 * - Returns an object containing the alerts to display as well as the status of the challenge : {roomID : { alert : " ", done : boolean}}.
 * - Alerts are strings (empty strings if there is no alert to display).
 * - The status of the challenge is a boolean value (true if the challenge is successful or false if it is in progress or incorrect).
 * @memberof Tabletop
 */
const checkPosition = function () {
  // In the real implementation there will be a client request which will return the true data
  return {
    room1: {
      alert: "gate-open", // alert : "" si pas d'alerte à envoyer
      done: true
    },
    room2: {
      alert: "recipe-not-ok",
      done: true // il faudrait que si l'enfant décide de refaire un gateau, done revienne à false puis true quand le deuxième gateau est fait
    },
    room3: {
      alert: "combination-not-ok",
      done: true // il faudrait que ça renvoie true chaque fois qu'un défi est réussi puis revienne à false pour le défi suivant
    },
    room4: {
      alert: "not-a-vehicle",
      done: true
    }
  }
}

/** Returns the high-level state of Tabletop.
 * - Returns an object containing names (strings) and positions (integers) of Tabletop elements. 
 * - Elements names correspond to images names to display on the canvas.
 * - Images must be referenced in the html page between img tags. 
 * - The position type depends on the grid type.
 * @memberof Tabletop
 */
const getTabletopObjects = function () {
  // In the real implementation there will be a client request which will return the true data
  return {
    room1: {
      robot: { name: "ozobot", x: 2, y: 0, angle: 0 }, // Here the location on the 4x4 grid
      instructions: [
        { name: "forward2", position: 8 }, // Here the position on the left panel "programming" area
        { name: "forward1", position: 7 },
        { name: "turnRight", position: 6 },
        { name: "forward3", position: 5 },
        { name: "turnRight", position: 4 },
        { name: "forward3", position: 3 },
        { name: "start", position: 0 }
      ]
    },
    room2: {
      bowl: [
        { name: "addChocolat", position: 0 },
        { name: "addFlour", position: 1 },
        { name: "addSugar", position: 3 },

      ],
      dish: [
        { name: "addEggs", position: 0 },
        { name: "addPear", position: 1 },
        { name: "addMilk", position: 2 },
        { name: "addOil", position: 3 }
      ],
      mold: [
        { name: "doPour", position: 0 },
        { name: "doMix", position: 2 }
      ],
      oven: [
        { name: "whileSoftCake", position: 0 },
        { name: "doBake", position: 2 }
      ],
    },
    room3: {
      squares: [
        { name: "redSquare", x: 0, y: 2 },
        { name: "redSquare", x: 0, y: 1 },
        { name: "redSquare", x: 0, y: 3 },
        { name: "redSquare", x: 0, y: 4 },
        { name: "redSquare", x: 0, y: 0 },
        { name: "redSquare", x: 1, y: 0 },
        { name: "redSquare", x: 2, y: 0 },
        { name: "redSquare", x: 3, y: 0 },
        { name: "redSquare", x: 4, y: 0 },
        { name: "redSquare", x: 2, y: 2 },
        { name: "redSquare", x: 4, y: 2 },
        { name: "redSquare", x: 4, y: 1 },
        { name: "redSquare", x: 4, y: 3 },
        { name: "redSquare", x: 4, y: 4 }
      ],
      numbers: [
        { name: "0", position: 0 },
        { name: "0", position: 1 },
        { name: "0", position: 2 },
        { name: "0", position: 3 },
        { name: "1", position: 5 },
        { name: "1", position: 8 },
        { name: "1", position: 12 },
        { name: "1", position: 15 },
        { name: "1", position: 18 },
        { name: "0", position: 20 },
        { name: "0", position: 24 }
      ]
    },
    room4: {
      vehicle: [
        { name: "red", x: 0, y: 0 },
        { name: "blackEyeHorizontal", x: 1, y: 0 },
        { name: "blueSwitch", x: 2, y: 0 },
        { name: "whiteWheelVertical", x: 3, y: 0 },
        { name: "red", x: 0, y: 1 },
        { name: "blackEyeHorizontal", x: 0, y: 2 },
        { name: "blueSwitch", x: 0, y: 3 },
        { name: "red", x: 1, y: 1 },
        { name: "blackEyeHorizontal", x: 3, y: 2 },
        { name: "blueSwitch", x: 3, y: 3 }
      ]
    }
  };
}

/** Draw and display the initial version of the canvas.
 * - Hides certain elements of the html page.
 * - Draw and display the canvas with a background image and the defined parameters (position, size).
 * @memberof Canvas
 */
const drawCanvas = function () {
  for (let element in canvasHiddenData) {
    document.getElementById(canvasHiddenData[element]).style.display = "none";
  }
  let background = document.getElementById(backgroundID);
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
  canvas.style.display = "block";
}

/** Draw and display each Tabletop element on the canvas.
 * - For each room, this function draws the sprites according to predefined positions and stored in objects by the initialization functions.
 * @memberof Canvas
 * @param {object} sprites Object returned by getTableTopObjects() function containing the Tabletop elements.
 */
const drawObjects = function (sprites) {
  if (roomID == "room1") {
    let instructions = sprites[roomID]["instructions"], robot = sprites[roomID]["robot"];
    robot.width = ozobotInitData["width"], robot.height = ozobotInitData["height"];
    for (let instruction of instructions) {
      ctx.drawImage(document.getElementById(instruction.name), panel[instruction.position].x, panel[instruction.position].y, panel[instruction.position].width, panel[instruction.position].height);
    }
    if (robot.x > 1 && robot.y > 1) {
      robot.name = ozobotInitData["transparentBot"];
    }
    ctx.save();
    ctx.translate(xGrid[robot.x] + (robot.width / 2), yGrid[robot.y] + (robot.height / 2));
    ctx.rotate(robot.angle * (Math.PI / 180));
    ctx.translate(-(xGrid[robot.x] + (robot.width / 2)), -(yGrid[robot.y] + (robot.height / 2)));
    ctx.drawImage(document.getElementById(robot.name), xGrid[robot.x], yGrid[robot.y], robot.width, robot.height);
    ctx.restore();
  } else if (roomID == "room2") {
    let objects = sprites[roomID], width = canvas.width * 0.16, height = canvas.height * 0.1;;
    for (let element in objects) {
      for (let i = 0; i < objects[element].length; i++) {
        let name = objects[element][i].name;
        let position = objects[element][i].position;
        ctx.drawImage(document.getElementById(name), whiteBoard[element][position].x, whiteBoard[element][position].y, width, height);
      }
    }
  } else if (roomID == "room3") {
    let squares = sprites[roomID]["squares"], numbers = sprites[roomID]["numbers"];
    for (let square of squares) {
      square.width = canvas.width * 0.06, square.height = canvas.height * 0.07;
      ctx.drawImage(document.getElementById(square.name), xGrid[square.x], yGrid[square.y], square.width, square.height);
    }
    for (let number of numbers) {
      ctx.drawImage(document.getElementById(number.name), numberGrid[number.position].x, numberGrid[number.position].y, numberGrid[number.position].width, numberGrid[number.position].height);
    }
  } else if (roomID == "room4") {
    let vehicle = sprites[roomID]["vehicle"], width = canvas.width * 0.08, height = canvas.height * 0.12;
    for (let cube of vehicle) {
      ctx.drawImage(document.getElementById(cube.name), xGrid[cube.x], yGrid[cube.y], width, height);
    }
  }
}

/** Initialize the left programming panel of the room 1. 
 * - The global array contains positions and sizes of each panel box. 
 * - The room1 instructions will be drawn on these boxes. 
 * - The index of each object containing in the array corresponds to the position returned by the getTabletopObjects() function. 
 * @memberof Canvas
 */
const initPanel = function () {
  let x = canvas.width * 0.01, y = canvas.height * 0.9, width = canvas.width * 0.17, height = canvas.height * 0.111;
  for (let i = (panel.length - 1); i >= 0; i--) {
    if (i == 0) {
      x = canvas.width * 0.2;
      width = canvas.width * 0.08;
    }
    panel[i] = { x: x, y: y - (i * height), width: width, height: height };
  }
}

/** Initialize the grid of the room 1, 3 and 4. 
 * - Each global array contains the list of x or y coordinates.
 * - The instructions will be drawn at these coordinates. 
 * - The index of each coordinate containing in the array corresponds to the position returned by the getTabletopObjects() function. 
 * - The coordinates of the position (0;0) and the coefficients are defined in global variables specific to each room.
 * @memberof Canvas
 */
const initGrid = function () {
  xGrid[0] = x0; yGrid[0] = y0;
  for (let i = 1; i < xGrid.length; i++) {
    xGrid[i] = xGrid[i - 1] - xCoef;
    yGrid[i] = yGrid[i - 1] - yCoef;
  }
}

/** Initialize the white board of the room 2. 
 * - The global object contains an object for each part of the white board which contain coordinates of each panel box. 
 * - The room3 instructions will be drawn on these boxes. 
 * - The index of each object containing in the array corresponds to the position returned by the getTabletopObjects() function. 
 * @memberof Canvas
 */
const initWhiteBoard = function () {
  whiteBoard = {
    "bowl": [
      { x: 0.028, y: 0.46 },
      { x: 0.19, y: 0.46 },
      { x: 0.028, y: 0.57 },
      { x: 0.19, y: 0.57 }
    ],
    "dish": [
      { x: 0.028, y: 0.72 },
      { x: 0.19, y: 0.72 },
      { x: 0.028, y: 0.83 },
      { x: 0.19, y: 0.83 }
    ],
    "mold": [
      { x: 0.468, y: 0.535 },
      { x: 0.468, y: 0.645 },
      { x: 0.468, y: 0.755 }
    ],
    "oven": [
      { x: 0.778, y: 0.535 },
      { x: 0.778, y: 0.645 },
      { x: 0.778, y: 0.755 }
    ]
  }

  for (let name in whiteBoard) {
    for (let i = 0; i < whiteBoard[name].length; i++) {
      whiteBoard[name][i].x = whiteBoard[name][i].x * canvas.width;
      whiteBoard[name][i].y = whiteBoard[name][i].y * canvas.height;
    }
  }
}

/** Initialize the control panel of the room 3. 
 * - The global array contains positions and sizes of each panel box. 
 * - The room3 instructions will be drawn on these boxes. 
 * - The index of each object containing in the array corresponds to the position returned by the getTabletopObjects() function. 
 * @memberof Canvas
 */
const initControlPanel = function () {
  initGrid();
  let x = canvas.width * 0.885, y = canvas.height * 0.32, width = canvas.width * 0.033, height = canvas.height * 0.045;
  for (let i = 0; i < numberGrid.length; i++) {
    numberGrid[i] = { x: x - (i * (width + (canvas.width * 0.00133))), y: y, width: width, height: height };
  }

}

/** Reset the canvas to do another challenge in the same room.
 * - Change the audio and written instructions according to the new challenge to be solved.
 * - Reset canvas display.
 * @memberof Canvas
 */
const playAgain = function () {
  if (roomID == "room2") {
    document.getElementById(free["buttonID"]).style.display = "none";
    free["audioSource"] = free["audioSource2"];
    document.getElementById(free["gobID"]).style.display = "block";
    document.getElementById(free["gobOnclick"]).style.display = "none";
    document.getElementById(free["audioID"]).src = free["audioReplay"];
    document.getElementById(free["textID"]).dataset.slug = free["textReplay"];
    slugstring.fill(strings_of_the_html_page);
    replay(free["audioID"]);
    initWhiteBoard();
    initGame();
  } else if (roomID == "room3" && replayed == 0) {
    document.getElementById(free["audioID"]).src = free["audioReplay"];
    // @vthierry replay(free["audioID"]);
    document.getElementById(free["textID"]).dataset.slug = free["textReplay1"];
    slugstring.fill(strings_of_the_html_page);
    initControlPanel();
    initGame();
  } else if (roomID == "room3" && replayed == 1) {
    document.getElementById(free["audioID"]).src = free["audioReplay2"];
    document.getElementById(free["textID"]).dataset.slug = free["textReplay2"];
    slugstring.fill(strings_of_the_html_page);
    replay(free["audioID"]);
    initControlPanel();
    initGame();
  }
}

/** Initializes the game and calls the functions necessary to update the display and alerts.
 * - The timer checks the status of the tabletop every second.
 * - The timer stops when the challenge is successful.
 * - For each alert title transmitted by the tabletop, a corresponding onomatopoeia is assigned and displayed in the body of the notification.
 * @memberof Canvas
 */
const initGame = function () {
  initGame2(); // @vthierry to interface with the new panel
}

/** Initializes the game and calls the functions necessary to update the display and alerts.
 *  - This using the original mechanism.
 * @memberof Canvas
 */
const initGame1 = function () {
  // Implements the regular call of display update
  let event = checkPosition(), exit = event[roomID]["done"], alert = event[roomID]["alert"], notificationOptions = {};
  drawCanvas();
  drawObjects(getTabletopObjects());
  let timer = setInterval(function () {
    console.log(new Date()); // Just for test
    if (hasTabletopChanged()) {
      drawCanvas();
      drawObjects(getTabletopObjects());
      event = checkPosition();
    }
    if (exit) {
      clearInterval(timer);
      exitGame();
    }
    if (alert != "") {
      if (alert == "gate-open") {
        notificationOptions.body = "Clic-Clac";
      } else if (alert == "wall-crash") {
        notificationOptions.body = "Bam ! Ouch...";
      } else if (alert == "trap-fall") {
        notificationOptions.body = "Ahhhhh.... Splash !";
      } else if (alert == "recipe-ok") {
        notificationOptions.body = "Yum... Yum...";
      } else if (alert == "recipe-not-ok") {
        notificationOptions.body = "Yuck !";
      } else if (alert == "combination-not-ok") {
        notificationOptions.body = "Gzzzt";
      } else if (alert == "combination-ok") {
        notificationOptions.body = "Gzzzt Clic-Clac";
      } else if (alert == "not-a-vehicle") {
        notificationOptions.body = "Bip";
      } else if (alert == "rooling-vehicle") {
        notificationOptions.body = "Vroom !";
      }
      let notification = new Notification(alert, notificationOptions);
    }
  }, 1000); // Called every second
}


/** Finishess the game and calls the functions necessary to update the display and alerts.
 * @memberof Canvas
 */
const exitGame = function() {
  canvas.style.display = "none";
  if (roomID == "room2" && replayed == 0) {
    document.getElementById(free["audioID"]).src = free["audioSource"]; // @vthierry to avoid play() failed because the user didn't interact with the document first.
    document.getElementById(free["textID"]).dataset.slug = free["textFree1"];
    slugstring.fill(strings_of_the_html_page);
    replay(free["audioID"]);
    document.getElementById(free["buttonID"]).style.display = "block";
    document.getElementById(free["gobID"]).style.display = "none";
    document.getElementById(free["gobOnclick"]).style.display = "block";
    replayed += 1;
  } else if (roomID == "room3" && replayed < 2) {
    playAgain();
    replayed += 1;
  } else {
    document.getElementById(free["audioID"]).src = free["audioSource"];  // @vthierry to avoid play() failed because the user didn't interact with the document first.
    document.getElementById(free["textID"]).dataset.slug = free["textFree"];
    replay(free["audioID"]);
    slugstring.fill(strings_of_the_html_page);
    document.getElementById(free["buttonID"]).style.display = "block";
  }
}

/** Initializes the game and runs it.
 *  - Thus using the external mechanism.
 * @memberof Canvas
 */
const initGame2 = function () {
   window.location.replace("./gamewindow.html?roomID=" + roomID);
}
// => on return go to exitGame
  if (new URLSearchParams(window.location.search).get("exitGame") == "please") {
    document.getElementById("startGame").style.display = "none";
    exitGame();
  }

//// Main ////

var roomID = document.getElementsByTagName("body")[0].id;
