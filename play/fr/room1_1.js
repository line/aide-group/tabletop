/**
 * - Initializes the global variables specific to the operation of the canvas for the room concerned.
 * @file 
 */

/**
 * - The global variables.
 * @namespace Variables
 */

/** Initialization of the canvas.
 * - In files : room1_1.js, room2_1.js, room3_1.js, room4_1.js.
 * - Create the canvas. 
 * @memberof Variables
 * @const {HTMLCanvasElement} canvas
 */
const canvas = document.querySelector("canvas");

/** Initialization of the context.
 * - In files : room1_1.js, room2_1.js, room3_1.js, room4_1.js.
 * - Create the canvas context. 
 * @memberof Variables
 * @const {CanvasRenderingContext2D} ctx 
 */
const ctx = canvas.getContext("2d");

// Sets the dimensions of the canvas so that it adapts to the size of the window.
canvas.width = window.innerWidth * 0.60;
canvas.height = window.innerHeight * 0.70;

// Initialization of global variables specific to the room1 simulator.
var ozobot, frame, trap, secretPath, barriers, doorA, doorB, exitDoor, exit, doors, closedDoor, key, unlocked, instructions = [];

// Creation of room-specific global variables.
var backgroundID = 'canvasBackground';
var canvasHiddenData = { trainingButtonID: 'startTraining', gameButtonID: 'startGame', simulatorID: 'simulator' };
var ozobotInitData = { ozobotImage: 'ozobot', transparentOzobot: 'ozobotOpacity0', x: canvas.width * 0.87, y: canvas.height * 0.72, xVelocity: 0, yVelocity: 0, angle: 0, width: canvas.width * 0.09, height: canvas.height * 0.14 }
var free = { audioID: 'audioOzon', audioSource: './audio/Room1_1FreeAudioSource.mp3', textID : 'ozonText', textFree : 'ozonTextRoom1_1Free', buttonID: 'continueButton' };
var panel = new Array(9), xGrid = new Array(4), yGrid = new Array(4), x0 = canvas.width * 0.87, y0 = canvas.height * 0.72, xCoef = x0 * 0.197, yCoef = y0 * 0.322;
