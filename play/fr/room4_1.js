/**
 * - Initializes the global variables specific to the operation of the canvas for the room concerned.
 * @file 
 */


/** Initialization of the canvas.
 * - Create the canvas. 
 * @memberof Variables
 * @ignore
 * @const {HTMLCanvasElement} canvas
 */
const canvas = document.querySelector("canvas");

/** Initialization of the context.
 * - Create the canvas context. 
 * @memberof Variables
 * @ignore
 * @const {CanvasRenderingContext2D} ctx 
 */
const ctx = canvas.getContext("2d");

// Creation of room-specific global variables.
var backgroundID = 'canvasBackground';
var canvasHiddenData = {gameButtonID: 'startGame'};
var free = { audioID: 'audioOzon', textID : 'ozonText', audioSource: './audio/Room4_1FreeAudioSource.mp3', textFree : 'ozonTextRoom4_1Free', buttonID: 'continueButton'}; 
var xGrid=new Array(4), yGrid=new Array(4), x0 = canvas.width * 0.35, y0 = canvas.height * 0.86, xCoef = x0 * 0.232, yCoef = y0 * 0.14;
