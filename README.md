# tabletop

The Tabletop activites RPi software

@aideAPI

## The Tabletop activities

This software package provides the software and middle-ware for the Tabletop [low-cost tabletop game to collect learning analytic during computational thinking using unplugged or tangible activities](https://hal.inria.fr/hal-03040909).

![Setup view](https://line.gitlabpages.inria.fr/aide-group/tabletop/view.png "Setup view")

### System architecture

![System architecture](https://line.gitlabpages.inria.fr/aide-group/tabletop/about-arch.png "System architecture")

The mechanical setup drives angular and linear actuators and motors in order to move the objects on the tabletop, and grabs images in order to analyze the game state. User interaction is performed via web pages accessible on a tactile screen, while video and audio recording of small interviews is also available. 

The software architecture includes a local client/server mechanism in order all user interface to be designed as web pages, while the server allows to run algorithms and store the collected data, before sending them into a safe remote storage on an external virtual machine. 

Hardware drivers and the client/server mechanisms runs on a RPi (Raspberry-3) low-cost standalone computer with a WiFi hot-spot allowing to drive local connected objects and connects to a Desktop computer, in order to connect to remote hosts.

The client/server mechanism is implemented in JavaScript (using node.js) wrapping algorithms implemented in C/C++, while hardware drivers are implemented in C or Python.This multi-language choice allows us to reuse any existing software component, have critical part of the code implemented in the most efficient language (in terms of performance, but also robustness and maintenance), and allows miscellaneous future development.

The user simply plays with the mechanical device and interact with a touch screen.

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/tabletop/view2.png "HTML web interface")

### Hardware documentation

All hardware elements are available [here](https://drive.google.com/drive/u/0/folders/12FkkwNJChk_dvSozy4UcniYDI-pJs4AS) and detailed in the [related publication](https://hal.inria.fr/hal-03040909), beyond this schematic description of the hardware design choices, using low-cost easily reusable hardware components:

![Hardware designed choices](https://line.gitlabpages.inria.fr/aide-group/tabletop/about-hardware-1.png "Hardware designed choices")

and this hardware photo gallery:

![Hardware photo gallery](https://line.gitlabpages.inria.fr/aide-group/tabletop/about-hardware-2.png "Hardware photo gallery")

- A: The Raspberry computer tablet computer mounted on a carriage with the image analysis camera Raspicam mounted on a pole to provide a top view of the gameplay space.
- B: A view of the worksite, showing that everything has been built with standard craft tools.
- C and D: Detailed views of an automatic gate between two rooms, with the controller stick in the cellar part of the box.
- E and F: Detailed views of the locker mechanism, the bolt fastener has been produced on a 3D printer in a maker-space.
- G: A view of the game board before putting it in place, all decoration is printed on a contact paper stuck on a thin wooden plank. 
- H and I: Detailed view of the wagon mechanism allowing the tablet to move along the game sequence.
- J: A view of the setup when closed, allowing to easily move it from one place to another.

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/tabletop'>https://gitlab.inria.fr/line/aide-group/tabletop</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/tabletop'>https://line.gitlabpages.inria.fr/aide-group/tabletop</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/tabletop/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/tabletop/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/tabletop'>softwareherirage.org</a>
- Version `0.0.3`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/tabletop.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aidecv: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidecv'>Image processing encapsulation via web interface</a></tt>
- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>aideweb: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aideweb'>Javascript server side node express utilities for web applications and service</a></tt>
- <tt>express: <a target='_blank' href='http://expressjs.com/'>Fast, unopinionated, minimalist web framework</a></tt>
- <tt>stepsolver: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/stepsolver'>A step by step variational solver mechanism</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Lola Denet&nbsp; <big><a target='_blank' href='mailto:lola.denet@etu.u-bordeaux.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www.linkedin.com/in/lola-denet-bioinfo'>&#128463;</a></big>
- Éric Pascual&nbsp; <big><a target='_blank' href='mailto:eric.g.pascual@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://twitter.com/ericpobot'>&#128463;</a></big>
- Sabrina Barnabé&nbsp; <big><a target='_blank' href='mailto:snjl-contact@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://snjl.fr'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
