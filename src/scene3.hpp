namespace tabletop {
  class DigitsParser {
    PrototypesComparer comparer_cells, comparer_digits;
    wjson::Value digit_areas;
    void construct()
    {
      if(!digit_areas.isRecord()) {
        wjson::Value& digits_area = getParameters()["rooms"][2]["digits"]["area"];
        wjson::Value& cells_area = getParameters()["rooms"][2]["cells"]["area"];
        double width = (digits_area.get("right", 0.0) - digits_area.get("left", 0.0)) / 25.0;
        digits_area["width"] = cells_area["width"] = rint(width);
        digits_area["height"] = cells_area["height"] = rint(digits_area.get("bottom", 0) - digits_area.get("top", 0));
        for(unsigned int ij = 0; ij < 25; ij++) {
          digit_areas[ij] = digits_area;
          digit_areas[ij]["color"] = "red";
          digit_areas[ij]["left"] = rint(digits_area.get("left", 0) + ij * width);
          digit_areas[ij]["right"] = digit_areas[ij].get("left", 0) + digits_area.get("width", 0);
        }
        comparer_cells.construct(3, "cells", false);
        comparer_digits.construct(3, "digits", false);
      }
    }
public:
    void parse(aidecv::Image& view)
    {
      construct();
      for(unsigned int ij = 0; ij < 25; ij++) {
        JSON digit_area = digit_areas[ij];
        view.set("{do: draw what:[" + digit_area.asString() + "]}");
        aidecv::Image area(digit_area, view);
        JSON result_cells = comparer_cells.get(area);
        if(result_cells.at(0).get("name", "") != "Z") {
          JSON result_digits = comparer_digits.get(area);
          view.parameters["parse"]["digits"][ij] = result_digits;
        } else {
          view.parameters["parse"]["digits"][ij] = result_cells;
        }
        view.set(aidesys::echo("{do: draw what:[{do: text x: %d y: %d text: \"" + view.parameters.at("parse").at("digits").at(ij).at(0).get("name", "") + "\" color:yellow}]}",
                               (digit_area.get("left", 0) + digit_area.get("right", 0)) / 2, (digit_area.get("top", 0) + digit_area.get("bottom", 0)) / 2));
      }
    }
  }
  digitsParser;

  class SquaresParser {
    PrototypesComparer comparer;
    wjson::Value square_areas;
    void construct()
    {
      if(!square_areas.isRecord()) {
        wjson::Value& squares_area = getParameters()["rooms"][2]["squares"]["area"];
        squares_area["width"] = rint((squares_area.get("right", 0.0) - squares_area.get("left", 0.0)) / 5.0);
        squares_area["height"] = rint((squares_area.get("bottom", 0.0) - squares_area.get("top", 0.0)) / 5.0);
        for(unsigned int j = 0; j < 5; j++) {
          for(unsigned int i = 0; i < 5; i++) {
            square_areas[j][i] = squares_area;
            square_areas[j][i]["color"] = "red";
            square_areas[j][i]["top"] = squares_area.get("top", 0) + j * squares_area.get("height", 0);
            square_areas[j][i]["bottom"] = squares_area.get("top", 0) + (j + 1) * squares_area.get("height", 0);
            square_areas[j][i]["left"] = squares_area.get("left", 0) + i * squares_area.get("width", 0);
            square_areas[j][i]["right"] = squares_area.get("left", 0) + (i + 1) * squares_area.get("width", 0);
          }
        }
        comparer.construct(3, "squares", false);
      }
    }
public:
    void parse(aidecv::Image& view)
    {
      construct();
      for(unsigned int j = 0; j < 5; j++) {
        for(unsigned int i = 0; i < 5; i++) {
          JSON square_area = square_areas[j][i];
          view.set("{do: draw what:[" + square_area.asString() + "]}");
          aidecv::Image area(square_area, view);
          JSON result = comparer.get(area);
          view.parameters["parse"]["squares"][i][j] = result;
          view.set(aidesys::echo("{do: draw what:[{do: text, x: %d y: %d text: \"" + result.at(0).get("name", "") + "\" color:yellow}]}",
                                 (square_area.get("left", 0) + square_area.get("right", 0)) / 2, (square_area.get("top", 0) + square_area.get("bottom", 0)) / 2));
        }
      }
    }
  }
  squaresParser;
}
