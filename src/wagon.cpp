#include "wagon.hpp"
#include "parameters.hpp"
#include "time.hpp"
#include "rpi.hpp"

#include <math.h>

#include <unistd.h>
#include <signal.h>

namespace tabletop {
  void wagonCommand(double left_wheel_velocity, double right_wheel_velocity)
  {
    static const unsigned int channel[] = {
      getParameters()["wagon"]["channel"].get(0, 0u),
      getParameters()["wagon"]["channel"].get(1, 0u)
    };
    rpiContinuousCommand(channel[0], left_wheel_velocity);
    rpiContinuousCommand(channel[1], right_wheel_velocity);
  }
  // Wagon watchdog variables
  unsigned int wagon_watchdog_index = 0, wagon_watchdog_child_id = 0;

  void wagonDrive(double wheel_calibrated_velocity, unsigned int command_duration, bool wait)
  {
    static const double offset[] = {
      getParameters()["wagon"]["offset"].get(0, 0.0),
      getParameters()["wagon"]["offset"].get(1, 0.0)
    };
    static const double gain[] = {
      getParameters()["wagon"]["gain"].get(0, 0.0),
      getParameters()["wagon"]["gain"].get(1, 0.0)
    };
    double
      left = gain[0] * wheel_calibrated_velocity + offset[0],
      right = gain[1] * wheel_calibrated_velocity + offset[1];
    // Registers this method call index for the watchdog mechanism and kill previous thread if any
    unsigned int this_index = ++wagon_watchdog_index;
    if(wagon_watchdog_child_id != 0) {
      kill(wagon_watchdog_child_id, SIGKILL);
      wagon_watchdog_child_id = 0;
    }
    wagonCommand(left, right);
    // Waits until command completion
    if(wait) {
      aidesys::sleep(command_duration);
      wagonCommand(offset[0], offset[1]);
    } else {
      // Starts a thread to implement the watchdog
      if(command_duration > 0) {
        fflush(stdout);
        switch((int) (wagon_watchdog_child_id = fork())) {
        // Error in the forking process
        case -1:
          aidesys::alert("illegal-state", "in tabletop::wagonDrive, cannot fork");
          break;
        // This code is executed in the child
        case 0:
        {
          aidesys::sleep(command_duration);
          // Stops the servo-motors unless another wagonDrive command has been issued
          if(wagon_watchdog_index == this_index) {
            wagonCommand(offset[0], offset[1]);
          }
          wagon_watchdog_child_id = 0;
          exit(0);
        }
        default:
          // The code remainder is executed by the parent
          break;
        }
      }
    }
  }
  void wagonShift(int distance)
  {
    static const double
      duration_command_level = getParameters()["wagon"].get("command_level", 0),
      duration2distance_offset = getParameters()["wagon"].get("duration2distance_offset", 0),
      duration2distance_gain = getParameters()["wagon"].get("duration2distance_gain", 0);
    unsigned int duration = (int) rint(duration2distance_gain * fabs(distance) + duration2distance_offset);
    wagonDrive(distance < 0 ? -duration_command_level : duration_command_level, duration, true);
  }
}
