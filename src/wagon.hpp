#ifndef __tabletop_wagon__
#define __tabletop_wagon__

namespace tabletop {
  /**
   * @class wagon
   * @description Specifies the wagon command high-level functions.
   * - These functions are accessible via the `tabletop::` prefix.
   */
  /**
   * @function wagonShift
   * @memberof wagon
   * @static
   * @description Moves the wagon a given distance in camera pixel.
   * @param {int} distance The displacement distance in pixel.
   */
  void wagonShift(int distance);

  /**
   * @function wagonRoom
   * @memberof wagon
   * @static
   * @description Moves the wagon to ta given tabletob room location.
   * @param {uint} distance The room index 1,2,3,4.
   * @param {uint} [loop=1] The maximal number of iteration.
   * @return {bool} True if the mechanism succeed, false if it failed.
   */
  bool wagonRoom(unsigned int index, unsigned int loop = 1);
}
#endif
