#ifndef __tabletop_command__
#define __tabletop_command__

namespace tabletop {
  /**
   * @function command
   * @class
   * @static
   * @description Implements an ASCII command and returns the answer.
   * @param {string} [command="h"] The command.
   * - The `command("h")` returns the command usage.
   * @return {string} The command answer.
   */
  std::string command(const char *command = "h");
}
#endif
