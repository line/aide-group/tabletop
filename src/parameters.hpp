#ifndef __tabletop_parameters__
#define __tabletop_parameters__

#include "Value.hpp"

namespace tabletop {
  /**
   * @function getParameters
   * @class
   * @static
   * @description Returns the tabletob parameters
   * - The tabletob parameters are defined in the [./src/tabletob.json](https://gitlab.inria.fr/line/aide-group/tabletop/blob/master/src/tabletop.json) file.
   * @return {JSON} The parameter data structure as a read-only reference.
   */
  wjson::Value& getParameters();
}
#endif
