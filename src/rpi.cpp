#include "rpi.hpp"
#include "python.hpp"

// Used for the Python wrapper
#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace tabletop {
  // Rpi command init flag
  bool rpiCommandInit = true;

  void rpiInitCommand()
  {
    if(rpiCommandInit) {
      aidesys::python("start");
      int err = PyRun_SimpleString("from adafruit_servokit import ServoKit\n"
                                   "kit = ServoKit(channels=16)\n");
      aidesys::alert(err != 0, "  illegal-state", "in tabletop::rpiInitCommand, unable to start the adafruit python servokit");
      rpiCommandInit = false;
    }
  }
  void rpiContinuousCommand(unsigned int channel, double value)
  {
#ifndef ON_RASPPI
    aidesys::alert("  illegal-state", "in tabletop::rpiContinuousCommand(%d, %f): not on a RaspPi", channel, value);
#endif
    rpiInitCommand();
    value = fmin(1, fmax(-1, value));
    if(aidesys::alert(16 <= channel, "  illegal-argument", "in tabletop::rpiContinuousCommand, undefined channel '%d'", channel)) {}
    int err = PyRun_SimpleString(aidesys::echo("kit.continuous_servo[%d].throttle = %f", channel, value).c_str());
    aidesys::alert(err != 0, "illegal-state", "in tabletop::rpiContinuousCommand, unable to run the command err = '%d'", channel, value, err);
  }
  void rpiAngularCommand(unsigned int channel, unsigned int value)
  {
#ifndef ON_RASPPI
    aidesys::alert("  illegal-state", "in tabletop::rpiAngularCommand(%d, %d) not on a RaspPi", channel, value);
#endif
    rpiInitCommand();
    aidesys::alert(16 <= channel, "  illegal-argument", "in tabletop::rpiAngularCommand, undefined channel '%d'", channel);
    aidesys::alert(180 < value, "  illegal-argument", "in tabletop::rpiAngularCommand, value out of range '%d'", value);
    int err = PyRun_SimpleString(aidesys::echo("kit.servo[%d].angle = %d", channel, value).c_str());
    aidesys::alert(err != 0, "illegal-state", "in tabletop::rpiAngularCommand, unable to run the command 'kit.servo[%d].angle = %d', err = '%d'", channel, value, err);
  }
}
