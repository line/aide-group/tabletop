#include <WrapperCCService.hpp>
#include "command.hpp"

// Implements the web service at the C/C++ level.
std::string wrap(const std::map < std::string, std::string > &query)
{
  auto it = query.find("command");
  if(it != query.end()) {
    printf("command= '%s'\n", it->second.c_str());
    std::string response = tabletop::command(it->second.c_str());
    printf("\t=> '%s'\n", response.c_str());
    return response;
  } else {
    return nowrap(query);
  }
}
