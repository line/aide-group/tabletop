#ifndef __tabletop_ozon__
#define __tabletop_ozon__

#include "std.hpp"
#include "Value.hpp"

namespace tabletop {
  /**
   * @class ozon
   * @description Specifies the ozon ozobot high-level functions.
   * - These functions are accessible via the `tabletop::` prefix.
   */

  /**
   * @function ozonForward
   * @memberof ozon
   * @static
   * @description Moves the ozon a given distance foward.
   * @param {uint} distance The approximate distance in mm; 1 mm corresponds to about 2 pixels.
   */
  void ozonForward(unsigned int distance);

  /**
   * @function ozonTurn
   * @memberof ozon
   * @static
   * @description Turns the ozon a given angle in degree.
   * @param {int} angle The angle in degree, positive in the trigonometric direction.
   */
  void ozonTurn(int angle);

  /**
   * @function ozonLight
   * @memberof ozon
   * @static
   * @description Lights one ozon led.
   * @param {uint} index The led mask, any combination of the following.
   * - 0x01: top
   * - 0x02: left
   * - 0x04: center-left
   * - 0x08: center
   * - 0x10: center-right
   * - 0x20: right
   * - 0x80: rear
   * @param {uchar} blue The blue color.
   * @param {uchar} green The green color.
   * @param {uchar} red The red color.
   * - The color is given as a [w0rgbycm] char for ([w]hite, [0]black, [r]ed, [g]reen, [b]lue, [y]ellow, [c]yan, [m]agenta) color.
   */
  void ozonLight(unsigned int index, unsigned char blue, unsigned char green, unsigned char red);
  void ozonLight(unsigned int index, char color);

  /**
   * @function ozonStop
   * @memberof ozon
   * @static
   * @description Stops all ozon commands.
   */
  void ozonStop();

  /**
   * @function ozonWhere
   * @memberof ozon
   * @static
   * @description Returns the ozon absolute position and orientation in the camera frame of reference.
   * @return {JSON} The estimated position.
   *  - `x` The Ozon horizontal final position, in pixel, -1 if undefined.
   *  - `y  The Ozon vertical final position, in pixel, -1 if undefined.
   *  - `a` The Ozon rotation, in degree, 0 if undefined.
   *  - `r` The Ozon apparent radius, in pixel, 0 in undefined.
   *  - `d` The local displacement in pixel to measure the orientation.
   */
  JSON ozonWhere();

  /**
   * @function ozonMove
   * @memberof ozon
   * @static
   * @description Moves ozon to an absolute position and orientation.
   * @param {uint} x The Ozon horizontal final position, in pixel.
   * @param {uint} y The Ozon vertical final position, in pixel.
   * @param {int} a The Ozon rotation, in degree;
   * - 0 if aligned with the horizontal line in the rightward direction,
   * - > 0 if oriented towards the top direction in the image.
   * @param {uint} [loop=1] The maximal number of iteration.
   */
  void ozonMove(unsigned int x, unsigned int y, int a, unsigned int loop = 1);

  /**
   * @function ozonCommand
   * @memberof ozon
   * @static
   * @description Sends a command to the ozon interface.
   * @param {string} command The command as defined by the [ozon-lib](https://gitlab.inria.fr/line/aide-group/ozobot-lib) python interface, i.e.:
   * - `ozon._send_led_control_command(index, red, green, blue)`
   * - `ozon.drive(left_velocity_0_from_999, right_velocity_0_from_999, delay_milli_second, wait_true_or_false)`
   * - `ozon.spin(velocity_0_from_999, delay_milli_second, wait_true_or_false)`
   */
  void ozonCommand(String command);
}
#endif
