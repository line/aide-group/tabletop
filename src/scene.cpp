#include "scene.hpp"
#include "parameters.hpp"

#include "scene0.hpp"
#include "scene1.hpp"
#include "scene2.hpp"
#include "scene3.hpp"
#include "scene4.hpp"

namespace tabletop {
  // Image input parameters
  unsigned int getView_device = -1;
  wjson::Value getView_rectification, getView_balance, getView_spot;
  void getSceneInit()
  {
    if((int) getView_device == -1) {
      getView_device = getParameters().at("camera").get("device", 0);
#ifndef ON_RASPPI
      aidesys::alert("  illegal-state", "in tabletop::getView, not on a RaspPi, thus device set to 0");
      getView_device = 0;
#endif
      getView_spot = getParameters().at("camera").at("spot");
      getView_spot["device"] = getView_device;
      getView_balance = getParameters().at("camera").at("balance");
      getView_rectification = getParameters().at("camera").at("view");
    }
  }
  // Gets an image with preprocessing
  aidecv::Image& getView(bool with_spot)
  {
    getSceneInit();
    static aidecv::Image grab, result;
    if(with_spot && getView_spot.isRecord()) {
      static bool once = true;
      if(once) {
        getView_spot["what"] = "init";
        grab.set(getView_spot);
        once = false;
      }
      getView_spot["what"] = "register";
      grab.set(getView_spot);
      getView_spot["what"] = "next-move";
      grab.set(getView_spot);
      getView_spot["what"] = "next-still";
      grab.set(getView_spot);
    } else {
      grab.set(getView_device);
    }
    if(getView_balance.isRecord()) {
      grab.set(getView_balance);
    }
    if(getView_rectification.isRecord()) {
      result.set(getView_rectification, grab);
    } else {
      result.set(grab);
    }
    return result;
  }
  // Translate the view at the software level
  aidecv::Image& setViewShift(unsigned int room)
  {
    static aidecv::Image result;
    aidesys::alert(room < 1 || 4 < room, "illegal-argument", "in tabletop::setViewShift, undefined room number '%d'", room);
    unsigned int shift = 0;
    // Estimates the shift, matching two regions of interest
    {
      JSON crop = getParameters()["rooms"][room - 1]["shift_reference_area"];
      // Considers the present grabbed view and a reference view roi
      aidecv::Image view = getView();
      aidecv::Image reference(getParameters()["rooms"][room - 1].get ("base", "") +
                              getParameters()["rooms"][room - 1].get("empty_view", "")), roi;
      if(crop.isRecord()) {
        result.set(crop, view);
        roi.set(crop, reference);
      } else {
        result = view;
        return result;
      }
      // Estimates the shift between both areas
      {
        JSON match = getParameters().at("camera").at("shift_match");
        result.set(match, roi);
        shift = result.parameters.at("match").at("transform").get("translation_i", 0);
        printf("shift = %d\n", shift);
      }
      // Reports the shift in the rectification
      {
        getView_rectification = getParameters()["camera"]["view"];
        std::string names[] = { "top_x", "right_x", "bottom_x", "left_x" };
        for(auto name : names) {
          getView_rectification[name] = getView_rectification.get(name, 0) - shift;
        }
      }
    }
    return result;
  }
  aidecv::Image& getScene(unsigned int room, bool with_event_detection)
  {
#ifdef ON_RASPPI
    aidecv::Image& result = getView(with_event_detection);
#else
    static aidecv::Image result;
    result.set(aidesys::echo("upload/test-view-%d.png", room));
#endif
    switch(room) {
    case 1:
      buttonParser.parse(result);
      blocksParser.parse(result);
      break;
    case 2:
      break;
    case 3:
      digitsParser.parse(result);
      squaresParser.parse(result);
      break;
    case 4:
      break;
    default:
      aidesys::alert("illegal-argument", "in tabletop::getScene, undefined room number '%d'", room);
      break;
    }
#if 1
    // Used for debug
    {
      result.set(aidesys::echo("{do: save path: ./upload/result/scene%d}", room));
    }
#endif
    return result;
  }
}
