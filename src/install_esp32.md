![AIDE logo](./etc/aide-logo.png) 

This is the AIDE installation tutorial on the ESP32 cards

The AIDE system drives servos and other effectors or read sensors via adafruit cards on a esp32 wifi interface, using the [esp32-roomcontrol](https://gitlab.inria.fr/line/aide-group/esp32-roomcontrol) middleware.

## Configuring the arduino IDE environment

- To download and install the [Arduino IDE](https://www.arduino.cc/en/main/software) either consider this [li nk](https://www.arduino.cc/en/main/software) or use your package installer, e.g., 
  - _sudo dnf install arduino_
  - Start arduino
    - Following the internal instructions, adds your login account the _dialout_ and _lock_ groups in order to access _/dev/ttyUSB0_ devices
      - This is done automatically in `/etc/group` but you must enter the root password

- Installing the ESP32 board support and Adafruit PWM servo drive library
  - Start arduino
  - Open File => Preferences => Settings
    - Enter "https://dl.espressif.com/dl/package_esp32_index.json" into Additional Board Manager URLs field. 
    - You can add multiple URLs, separating them with commas.
  - Open Tools => Boards => Board Manager window
    - Search "sp32"in the upper bar
    - Install esp32 by Espressif Systems
  - In Tools  => Boards 
    - Select "ESP32 Dev Module"
    - Install
  - In Sketch => Include Library => Manage Libraries
    - Select "Adafruit PWM servo driver library"
    - Install

- Installing the SPIFFS file upload plugin. In the Arduino sketchbook directory, following this [tutorial](https://randomnerdtutorials.com/install-esp32-filesystem-uploader-arduino-ide) 
  - _mkdir -p tools; cd tools ; wget https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/download/1.0/ESP32FS-1.0.zip ; unzip ESP32FS-1.0.zip ; rm ESP32FS-1.0.zip_

## Installing the software

- Installing the [esp32-roomcontrol](https://gitlab.inria.fr/line/aide-group/esp32-roomcontrol). In the Arduino sketchbook directory: 
  - _git clone https://gitlab.inria.fr/line/aide-group/esp32-roomcontrol.git roomctrl_
  - Set the wifi essid/passwd :
    - _cd Arduino/tools/roomctrl/data ; cp secrets-dist.conf secrets.conf_
    - Edit secrets.conf in order to set the wifi essid and password to be usedm e.g.:
      - `wifi.ssid=aide_tabletop`
      - `wifi.password=aide,moi,tabletop,t,aidera`

- Compiling and loading
  - Connect the ESP32/AdaFruit mount via the USB cable
  - Start Arduino
  - In Tools => Port select /dev/ttyUSB0
  - In File => Open, browse "tools/roomctrl" and select "roomcrtl.ino"
  - With Tools => ESP32 Sketch data Upload, loads the configuration files
  - With Sketch => Verify/Compile and Upload, compiles and load the driver

- Updating the circuit IP
  - The command _avahi-browse -atp | grep roomctrl_ allows to detect the circuits and get the IP
  - The ESP32 IPs are defined in following constant of the _src/tabletob.json_ file:
```  
  "esp32" : {
    // Left roomctrl_0 (room 1 and 2) and right roomctrl_1 (room 3 and 4) ESP32 IP
    "roomctrl_0_IP" : "192.168.4.14:8000",
    "roomctrl_1_IP" : "192.168.4.6:8000",
    // Left roomctrl_0 (room 1 and 2) and right roomctrl_1 (room 3 and 4) ESP32 MAC address
    "roomctrl_0_MAP" : "B4:E6:2D:A7:10:A5",
    "roomctrl_1_MAP" : "B4:E6:2D:A9:14:E1"
  },
```
  - The `sudo arp-scan --interface=wlan0 --localnet` returns the corresponding IP / MAC table

- Validating and testing
  - In Tools => Serial Monitor you can visualize the server dump output
    - Get the IP address, e.g., 192.168.0.6 or 192.168.0.7
    - Run _curl http://192.168.0.6:8000_ to verify
  - You may also use mDNS :
    - Run _avahi-browse -aplr_ to verify get the IPs associated to each service
   - Run _curl http://roomctrl-0.local:8000_ to verify
  - To directly test a servo, e.g., channel 1, position=90
    - Run once, to enable PWM, _curl -X POST -d 'action=enable' http://192.168.0.6:8000/pwm_
    - Run once, to verify settings, _curl http://192.168.0.6:8000/settings_
    - Run _curl -X POST -d 'position=90' http://192.168.0.6:8000/servos/1_
