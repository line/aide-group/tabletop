#ifndef __tabletop_room__
#define __tabletop_room__

namespace tabletop {
  /**
   * @class room
   * @description Specifies the room command functions.
   * - These functions are accessible via the `tabletop::` prefix.
   */
  /**
   * @function coverSet
   * @memberof room
   * @static
   * @description Opens or closed a room cover.
   * @param {uint} room The room number, from 1 to 4.
   * @param {bool} open_else_closed If true open, else close.
   */
  void coverSet(unsigned int room, bool open_else_closed);

  /**
   * @function doorSet
   * @memberof room
   * @static
   * @description Opens or closed a door.
   * @param {uint} room The room number, from 1 to 4.
   * @param {bool} open_else_closed If true open, else close.
   */
  void doorSet(unsigned int room, bool open_else_closed);

  /**
   * @function gateSet
   * @memberof room
   * @static
   * @description Sets the labyrinth gate in a given position.
   * @param {bool} a_else_b If true put in position A else put in position B.
   */
  void gateSet(bool a_else_b);
}
#endif
