#include "parameters.hpp"
#include "file.hpp"
#include "regex.hpp"

namespace tabletop {
  wjson::Value& getParameters()
  {
    static wjson::Value parameters;
    if(!parameters.isRecord()) {
      parameters = wjson::string2json(aidesys::load("./src/parameters.json"));
      parameters["rooms"] = wjson::string2json(aidesys::load("./src/parameters-rooms.json"));
    }
    return parameters;
  }
}
