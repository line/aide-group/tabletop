#!/bin/bash

# /**
#  * @function rmake
#  * @memberof etc
#  * @static
#  * @description Remote control of the RPi system
#  * ```
#  *     Usage: ./etc/rmake.sh $action
#  *     Actions:
#  *       connect:   Connects to the RPi.
#  *       show:      Shows the RPi tabletop web server page on the desktop computer
#  *       rshow:     Shows the RPi tabletop web server page on the RPi
#  *       start:     Starts the local web server, on the RPi.
#  *       stop:      Stops the different (run, show, disp) services processes, on the RPi.
#  *       wlan:      Swicth to the aide_tabletop wifi network.
#  * ```
#  */

# Switchs to the root directory
R0="`realpath $0`" ; ROOT="`dirname $R0`" ; ROOT="`dirname $ROOT`" ; cd $ROOT

# Gets a parameter form the parameters.json file
getParameter() {
  grep "$1" ./src/parameters.json | head -1 | sed 's/^[^:]*: *"\([^"]*\)".*$/\1/'
}

# Runs the different commands

case "$1" in

  # Runs connected commands
  connect | show | start | stop | upload )

    # Obtains the Rpi IP
#    if [ -z "`nmcli connection show --active | grep aide_tabletop`" ] 
#    then
#      RPI_MAC=`getParameter RPi_MAC_eth0`
#      RPI_IP=`sudo arp-scan --interface=wlan0 --localnet | grep $RPI_MAC | awk '{print $1}'`
#    else
#      RPI_IP=`getParameter RPi_IP`
#    fi
  RPI_IP=192.168.4.1

    # Checks the RPi connection
    if ssh -o ConnectTimeout=5 pi@$RPI_IP echo ok > /dev/null ; then ok=
    else echo "Unable to connect to the RPi IP='$RPI_IP'" ; exit
    fi

    case "$1" in

      # Connects via ssh
      connect )
        ssh -A -t pi@$RPI_IP 'cd Downloads/tabletop ; /bin/bash'
      ;;

      # Shows the RPi web page
      show )
        if [ -z "$BROWSER" ] ; then BROWSER='echo Open in your browser: ' ; fi
        $BROWSER http://$RPI_IP:8080/
      ;;
      rshow )
        ssh pi@$RPI_IP "cd ./Downloads/tabletop/src/tabletop.desktop ./Desktop"
        ssh pi@$RPI_IP "export DISPLAY=:0.0 ; chromium-browser http://192.168.1.4:8080"
# Also adding : echo '@/usr/bin/chromium-browser http://192.168.1.4:8080' >> /etc/xdg/lxsession/LXDE-pi/autostart
      ;;

      # Runs the server or stop it
      start | stop )
        # Stops the different rmake service
        ssh pi@$RPI_IP "sudo killall -q node /usr/lib/chromium-browser/chromium-browser-v7 raspistill"

        case "$1" in
          start )
            ssh pi@$RPI_IP "cd ./Downloads/tabletop ; npm start"
          ;;
        esac
      ;;

      # Uploads the data
      upload )
        rsync --rsh='ssh -C' --quiet --archive pi@$RPI_IP:/home/pi/Downloads/tabletop/upload .
      ;;

  esac
  ;;

  # Switch wifi to RPi local networl
  wlan )
    nmcli connection up aide_tabletop
  ;;

  # Shows usage if an undefined command
  * ) echo ; grep '^#  \*    ' $R0 | sed 's/#  \*     //' ;;
esac

# Runs subsequent commands if any

if [ \! -z "$2" ]
then 
 shift
 $R0 $*
fi

