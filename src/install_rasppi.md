![AIDE logo](./etc/aide-logo.png) 

This is the AIDE installation tutorial on a RPi Raspbian system (RaspPi)

_Notice_: 
- A [Raspbian SDcard image](https://files.inria.fr/mecsci/aide/Raspbian-AIDE.img.gz) with everything pre-installed is available.
  - It is to be copied on a _32Go fast_ (at least 10MB/s) SD-card.
  - It is to be installed following this [tutorial](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).
    - i.e., using the [balenaEtcher](https://www.balena.io/etcher/) software.
  - Then 
    - simply install this SDcard and start your RPi
    - runs _/home/pi/Downloads/aide/run.sh_
  - Initial connection is login: _pi_ password: _aide_

## Preparing the RaspPi system

- Download the [raspbian SDcard image](https://www.raspberrypi.org/downloads/raspbian/) and copy it on a _32Go fast (at least 10MB/s) SDcard_ following the [proposed tutorial](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).

- Let us expand and purge the file space, manually selecting _Advanced Options => Expand filesystem => Ok => Finish_ in order to get enough file space, when running:
  - _sudo raspi-config_ 

- Let us clean any unused package, mainly
 - _sudo apt-get -y purge wolfram-engine_
 - _sudo apt-get -y purge libreoffice*_

- Let us then clean and synchronize packages
  - _sudo apt-get -y clean_
  - _sudo apt-get -y autoremove_
  - _sudo apt-get -y update_
  - _sudo apt-get -y upgrade_

- Let us activate the [PiCamera](https://www.raspberrypi.org/documentation/configuration/camera.md), manually selecting _enable_ on the _camera configuration_ line, when running:
  - _sudo raspi-config_ 

- Let us activate the Raspad [virtual keyboard](https://raspberrypi.stackexchange.com/questions/41150/virtual-keyboard-activation)
  - _sudo apt-get install matchbox-keyboard_

- Let us activate the xdotool tool
  - _sudo apt-get install xdotool_

- Let us activate the [SSH remote access](https://www.raspberrypi.org/documentation/remote-access/ssh), manually selecting _Interfacing Options => SSH => Yes => Ok => Finish_, when running:
  - _sudo raspi-config_ 
  - To connect use _ssh pi@$IPADDRESS_ where the _$IPADDRESS_ is 
    - On the RPi the 1st value obtained running _hostname -I_.
    - On the local network _arp-scan --localnet_ or, e.g., _arp-scan 192.168.0.0/24_ allows to see IP/MAC association, given your RPi IP.
  - Then [passwordless](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md) ssh is easily implemented better than using:
    - _sshpass -p 'password' ssh pi@$IPADDRESS_

Then to connect we use _alias ssh-pi="ssh pi@`grep '^PI_IP=' $(AIDE_HOME)/src/test.sh | sed 's/^PI_IP=//'`"_

## Installing the AIDE bundle

- Let us first install dependencies used in our context
  - _sudo apt-get install git nodejs npm_
  - _sudo apt-get install python-devel_
  - _sudo npm -g install node-gyp_

- Packages are installed in the download directory, running:
  - _cd /home/pi/Downloads_
  - _git clone https://gitlab.inria.fr/line/aide-group/tabletop.git_
  - _git clone https://gitlab.inria.fr/line/aide-group/ozobot-lib.git_

  - _git clone https://gitlab.inria.fr/line/aide-group/ozobot-api.git_

- Installing the ozobot interface
  - _sudo pip3 install /home/pi/Downloads/ozobot-lib/dist/ozoevo_lib-0.1.0-py3-none-any.whl_
  - _sudo pip3 install /home/pi/Downloads/ozobot-api/dist/ozoevo_api-0.1.0-py3-none-any.whl_

- After installing the opencv and adafruit servo command libraries (see below), to start the AIDE service runs
  - _/home/pi/Downloads/aide/run.sh_

## Installing the opencv library

References : [here](https://learnopencv.com/install-opencv-4-on-raspberry-pi) or [there](https://www.pyimagesearch.com/2018/09/26/install-opencv-4-on-your-raspberry-pi).

- Let us first install dependencies used in our context
  - _sudo apt-get install build-essential cmake unzip pkg-config  libjpeg-dev libpng-dev libtiff-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libatlas-base-dev gfortran libgtk-3-dev libcanberra-gtk-module v4l-utils v4l-conf_

- Let us then download sources, presently [version 4.00](https://opencv.org/releases)
  - _cd /home/pi/Downloads ; git clone https://github.com/opencv/opencv.git ; cd opencv ; git checkout 4.0.0_
  - _cd /home/pi/Downloads ; git clone https://github.com/opencv/opencv_contrib.git ; cd opencv_contrib ; git checkout 4.0.0_

- Let us temporary increase the swap
  - sudo nano /etc/dphys-swapfile # To set CONF_SWAPSIZE=1024
  - sudo /etc/init.d/dphys-swapfile stop
  - sudo /etc/init.d/dphys-swapfile start

- Let us then compile and install opencv (about 1 hour compilation time)
  - _cd /home/pi/Downloads/opencv ; /bin/rm -rf build ; mkdir build ; cd build ; cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_TESTS=OFF -D INSTALL_PYTHON_EXAMPLES=OFF -D BUILD_EXAMPLES=OFF -D BUILD_opencv_java=OFF -D BUILD_opencv_python2=OFF -D BUILD_opencv_python3=OFF -D ENABLE_NEON=ON -D ENABLE_VFPV3=ON -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules .. ; make_
  - _sudo make install ; sudo ldconfig_

- Let us install the RaspiCam: C++ API
  - _cd /home/pi/Downloads_
  - _wget https://github.com/cedricve/raspicam/archive/master.zip_
  - _unzip master.zip ; rm master.zip_
  - _cd /home/pi/Downloads/raspicam-master ; /bin/rm -rf build ; mkdir build ; cd build ; cmake .. ; make ; sudo make install ; sudo ldconfig_

#### Remarks:

- There is no need to manipulate the swap or use several threads to compile on Rapberry3+.
- Here, opencv_contrib is not yet used in our context.
- Displaying the PiCamera on the screen with 3280x2464 image resolution:
  - _raspistill -t 0 -o /home/pi/Pictures/raspistill.jpg_
- Grabing an image at the maximal raspicam.h resolution saving with maximal quality jpg, after a 5s delay for image stabilization:
  - _raspistill -w 1280 -h 960 -q 100 -t 5 -o /home/pi/Pictures/raspistill.jpg_
- Displaying an image (after running _sudo apt-get -y install fbi_ to install):
  - _fbi -a /home/pi/Pictures/raspistill.jpg_
- Displaying the [USB camera](https://www.raspberrypi.org/documentation/usage/webcams):
  - _export DISPLAY=:0; vlc -f v4l2:///dev/video0_
- Tests if the PiCamera is connected to the /dev/video2 device (after _sudo apt-get install fswebcam_):
  - _fswebcam -S 5 --jpeg 100 -d /dev/video2 --no-banner --save test.jpg_
- Recording a video with sound (stooping with typing CTRL+C):
  - _ffmpeg -f alsa -i hw:0,0 -f video4linux2 -i /dev/video0 -f avi -vcodec mpeg4 -acodec libmp3lame output.avi_
- Installing a PiCamera GUI interface
  - _cd /home/pi/Downloads ; git clone https://github.com/jtpaquet/PiCamera-GUI_
  - _cd /home/pi/Downloads/PiCamera-GUI ; python3 ./picamera-gui/main.py_
- Recording audio using a USb microphone
  - _arecord -D sysdefault:CARD=1 -d 10 test.wav_

## Installing the adafruit servo command library

 The https://www.adafruit.com/product/3416 card is used, following the [tutorial](https://learn.adafruit.com/adafruit-16-channel-pwm-servo-hat-for-raspberry-pi) with [python wrapper usage](https://learn.adafruit.com/adafruit-16-channel-pwm-servo-hat-for-raspberry-pi/using-the-python-library).

- Configuring the i2c interface, manually selecting _Interfacing Options => I2C enable_ when running:
  - _sudo raspi-config_ 

- Installing adafruit [ServoKit](https://github.com/adafruit/Adafruit_CircuitPython_ServoKit) software
  - _sudo pip3 install adafruit-circuitpython-servokit_

### Remarks:

- To test the board and see the hat card connection
  - _sudo i2cdetect -y 1_
- To test a servo via the vanilla interface
  - _python3_
  - from adafruit_servokit import ServoKit
  - kit = ServoKit(channels=16)
  - kit.servo[10].angle = 0
- To enable/disable a servo
  - kit.servo[10].fraction = 1.0
  - kit.servo[10].fraction = None

## Wifi Access point installation

We need to activate the [wifi Access point](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md) access point, as a standalone network in order to drive the adafruit cards, without reconfiguring them when the network changes.

Then :

- We have to install these two packages
  - _sudo apt install dnsmasq hostapd_
  - _sudo systemctl stop dnsmasq ; sudo systemctl stop hostapd_
 
- Then use the `rpi_wifi_accesspoint.sh` file

### Remarks

 - We do not use [RaspAP](https://github.com/billz/raspap-webgui) as proposed [here](https://www.framboise314.fr/raspap-creez-votre-hotspot-wifi-avec-un-raspberry-pi-de-facon-express), despite it is very easy to use, because it is just a pain to ... turn the AP off after the install.

## Screen size

 - We use a [Raspad](https://raspad.sunfounder.com) device thus a 1280x800 pixels 10.1 inches screen, of size 22.1cm x 12.5cm
    - Considering a 2cm x 1.5cm finger precision, thus a 100 x 100 pixels zone, we have 11 x 8 clicable zones on a screen.

## ¡Not yet used! (unless develloping direct C/C++) servo interfaces:

- Installing dependencies
  - _sudo apt-get install libcurl4-openssl-dev autoconf automake libtool_

- Installing the [bcm 2835](http://www.airspayce.com/mikem/bcm2835) tool
  - _cd /home/pi/Downloads_
  - _wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.59.tar.gz_
  - _gunzip bcm2835-1.59.tar.gz ; tar xf bcm2835-1.59.tar ; rm bcm2835-1.59.tar_
  - _cd bcm2835-1.59 ; ./configure ; make ; sudo make install ; sudo ldconfig_

## Installing the tabletop service

- Installing the service
  - _sudo cp /home/pi/Downloads/tabletop/src/tabletop.service /etc/systemd/system_
  - _sudo systemctl daemon-reload_
  - _sudo systemctl enable tabletop_

+ Redone install if tabletop.service is installed or changes

- Restart the service if any web service file is changed
  - _sudo systemctl restart tabletop_
  - _sleep 1_
  - _sudo systemctl status tabletop_

- Verify the service
  - _journalctl -r -u tabletop.service_
- Clean the service log
  - _sudo journalctl --rotate --vacuum-time=1s_

- Auto starting the web browser (or edit by hand)
  - sudo echo '@/usr/bin/chromium-browser http://192.168.1.4:8080' >> /etc/xdg/lxsession/LXDE-pi/autostart

Ref: https://www.rickmakes.com/auto-run-an-application-on-raspberry-pi-raspbian-desktop/

## Saving the whole Raspbian-AIDE SDcard

- Power off the RPi, unplug the SDcard, and insert it in the desktop computer.

- On Linux:
  - Get the device ID of you SD card (check the size)
    - _sudo fdisk -l_
  - Make the diskimage (here $DEV is the found device ID using the previous command, e.g., /dev/mmcblk0)
    - _sudo dd bs=4M if=/dev/$DEV | gzip -q9 > Raspbian-AIDE.img.gz_

