#include "room.hpp"
#include "parameters.hpp"
#include "std.hpp"
#include "http.hpp"

namespace tabletop {
  bool esp32CommandInit[2] = { true, true };

  void esp32Command(unsigned int device, unsigned int channel, unsigned int value)
  {
#ifndef ON_RASPPI
    aidesys::alert("  illegal-state", "in tabletop::esp32Command(%d, %d, %d), not on a RaspPi", device, channel, value);
#endif
    aidesys::alert(2 <= device, "  illegal-argument", "in tabletop::esp32Command, undefined device '%d'", device);
    aidesys::alert(16 <= channel, "  illegal-argument", "in tabletop::esp32Command, undefined channel '%d'", channel);
    aidesys::alert(180 < value, "  illegal-argument", "in tabletop::esp32Command, value out of range '%d'", value);
    static const std::string deviceIP[] = {
      getParameters()["esp32"].get("roomctrl_0_IP", ""),
      getParameters()["esp32"].get("roomctrl_1_IP", "")
    };
    if(esp32CommandInit[device]) {
      aidesys::http(aidesys::echo("http://" + deviceIP[device] + "/pwm", channel), "action=enable");
      esp32CommandInit[device] = false;
    }
    // Implements the urgent stop command with device = -1
    if(device == (unsigned int) -1) {
      for(unsigned int device = 0; device < 2; device++) {
        aidesys::http(aidesys::echo("http://" + deviceIP[device] + "/pwm", channel), "action=disable");
        esp32CommandInit[device] = true;
      }
    }
    aidesys::http(aidesys::echo("http://" + deviceIP[device] + "/servos/%d", channel), aidesys::echo("position=%d", value), "POST");
  }
  void coverSet(unsigned int room, bool open_else_closed)
  {
    static const unsigned int device[] = {
      getParameters()["covers"]["device"].get(0, 0u),
      getParameters()["covers"]["device"].get(1, 0u),
      getParameters()["covers"]["device"].get(2, 0u),
      getParameters()["covers"]["device"].get(3, 0u),
      getParameters()["covers"]["device"].get(4, 0u),
      getParameters()["covers"]["device"].get(5, 0u),
      getParameters()["covers"]["device"].get(6, 0u),
      getParameters()["covers"]["device"].get(7, 0u)
    };
    static const unsigned int channel[] = {
      getParameters()["covers"]["channel"].get(0, 0u),
      getParameters()["covers"]["channel"].get(1, 0u),
      getParameters()["covers"]["channel"].get(2, 0u),
      getParameters()["covers"]["channel"].get(3, 0u),
      getParameters()["covers"]["channel"].get(4, 0u),
      getParameters()["covers"]["channel"].get(5, 0u),
      getParameters()["covers"]["channel"].get(6, 0u),
      getParameters()["covers"]["channel"].get(7, 0u)
    };
    static const unsigned int open_angle[] = {
      getParameters()["covers"]["open_angle"].get(0, 0u),
      getParameters()["covers"]["open_angle"].get(1, 0u),
      getParameters()["covers"]["open_angle"].get(2, 0u),
      getParameters()["covers"]["open_angle"].get(3, 0u),
      getParameters()["covers"]["open_angle"].get(4, 0u),
      getParameters()["covers"]["open_angle"].get(5, 0u),
      getParameters()["covers"]["open_angle"].get(6, 0u),
      getParameters()["covers"]["open_angle"].get(7, 0u)
    };
    static const unsigned int close_angle[] = {
      getParameters()["covers"]["close_angle"].get(0, 0u),
      getParameters()["covers"]["close_angle"].get(1, 0u),
      getParameters()["covers"]["close_angle"].get(2, 0u),
      getParameters()["covers"]["close_angle"].get(3, 0u),
      getParameters()["covers"]["close_angle"].get(4, 0u),
      getParameters()["covers"]["close_angle"].get(5, 0u),
      getParameters()["covers"]["close_angle"].get(6, 0u),
      getParameters()["covers"]["close_angle"].get(7, 0u)
    };
    if(aidesys::alert(!(0 < room && room <= 4), "  illegal-argument", "in tabletop::coverSet, bad room number '%d'", room)) {}
    unsigned int iroom = 2 * (room - 1);
    esp32Command(device[iroom], channel[iroom], open_else_closed ? open_angle[iroom] : close_angle[iroom]);
    esp32Command(device[1 + iroom], channel[1 + iroom], open_else_closed ? open_angle[1 + iroom] : close_angle[1 + iroom]);
  }
  void doorSet(unsigned int room, bool open_else_closed)
  {
    static const unsigned int device[] = {
      getParameters()["doors"]["device"].get(0, 0u),
      getParameters()["doors"]["device"].get(1, 0u),
      getParameters()["doors"]["device"].get(2, 0u),
      getParameters()["doors"]["device"].get(3, 0u)
    };
    static const unsigned int channel[] = {
      getParameters()["doors"]["channel"].get(0, 0u),
      getParameters()["doors"]["channel"].get(1, 0u),
      getParameters()["doors"]["channel"].get(2, 0u),
      getParameters()["doors"]["channel"].get(3, 0u)
    };
    static const unsigned int open_angle[] = {
      getParameters()["doors"]["open_angle"].get(0, 0u),
      getParameters()["doors"]["open_angle"].get(1, 0u),
      getParameters()["doors"]["open_angle"].get(2, 0u),
      getParameters()["doors"]["open_angle"].get(3, 0u)
    };
    static const unsigned int close_angle[] = {
      getParameters()["doors"]["close_angle"].get(0, 0u),
      getParameters()["doors"]["close_angle"].get(1, 0u),
      getParameters()["doors"]["close_angle"].get(2, 0u),
      getParameters()["doors"]["close_angle"].get(3, 0u)
    };
    aidesys::alert(!(0 < room && room <= 4), "  illegal-argument", "in tabletop::doorSet, bad room number '%d'", room);
    room--;
    esp32Command(device[room], channel[room], open_else_closed ? open_angle[room] : close_angle[room]);
  }
  void gateSet(bool a_else_b)
  {
    static const unsigned int
      device = getParameters()["gate"].get("device", 0u),
      channel = getParameters()["gate"].get("channel", 0u),
      a_angle = getParameters()["gate"].get("a_angle", 0u),
      b_angle = getParameters()["gate"].get("b_angle", 0u);
    esp32Command(device, channel, a_else_b ? a_angle : b_angle);
  }
}
