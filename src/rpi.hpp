#ifndef __tabletop_rpi__
#define __tabletop_rpi__

namespace tabletop {
  /**
   * @class rpi
   * @description Specifies the low-level functions for RPi servomotor control.
   * - These functions are accessible via the `tabletop::` prefix.
   */
  /**
   * @function rpiAngularCommand
   * @memberof rpi
   * @static
   * @description Sends a command to a the RPi local card servo mechanism.
   * - Interfaces with the RPi [adafruit servo kit](https://github.com/adafruit/Adafruit_CircuitPython_ServoKit) for the hat card, see [installation](https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/install_rasppi.md#installing-the-adafruit-servo-command-library).
   * @param {unint} channel The servo channel, 0 or 15 for the present hardware.
   * @param {unint} value The servo angle in degree, 0 to 180.
   */
  void rpiAngularCommand(unsigned int channel, unsigned int value);

  /**
   * @function rpiContinuousCommand
   * @memberof rpi
   * @static
   * @description Sends a command to a the RPi local card servo mechanism.
   * - Interfaces with the RPi [adafruit servo kit](https://github.com/adafruit/Adafruit_CircuitPython_ServoKit) for the hat card, see [installation](https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/install_rasppi.md#installing-the-adafruit-servo-command-library).
   * @param {unint} channel The servo channel, 0 or 15 for the present hardware.
   * @param {double} value The continuous servo velocity from -1 to 1.
   */
  void rpiContinuousCommand(unsigned int channel, double value);
}
#endif
