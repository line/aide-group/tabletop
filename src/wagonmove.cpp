#include "wagon.hpp"
#include "Controller.hpp"
#include "Image.hpp"

namespace tabletop {
  // Returns the wagon position room and position
  JSON wagonWhere()
  {
    static const unsigned int device = 4;
    aidecv::Image grab0(device);
    static const std::string blobs[] = { // blue, red, green, yellow
      "{do: blob blue: 255 green: 0 red: 0 hue_threshold: 20 saturation_threshold: 120 intensity_threshold: 120}",
      "{do: blob blue: 0 green: 0 red: 2550 hue_threshold: 20 saturation_threshold: 120 intensity_threshold: 120}",
      "{do: blob blue: 0 green: 255 red: 0 hue_threshold: 20 saturation_threshold: 120 intensity_threshold: 120}",
      "{do: blob blue: 128 green: 128 red: 0 hue_threshold: 20 saturation_threshold: 120 intensity_threshold: 120}"
    };
    static const unsigned int ledPositions[] = {
      100, 100, 100, 100
    };
    static wjson::Value result;
    result.clear();
    unsigned int imax = -1, smax = 0, xmax;
    for(unsigned int i = 0; i < 4; i++) {
      grab0.set(blobs[i]);
      grab0.set("{do: segment input: blob");
      grab0.set(aidesys::echo("{do: save path: wagonmove/grab%d}", i));
      JSON blob0 = grab0.parameters.at("segment").at(0);
      result["more"][i] = blob0;
      if(blob0.get("size", 0) > (int) smax) {
        imax = i, smax = blob0.get("size", 0), xmax = blob0.get("x", 0);
      }
    }
    result["room"] = imax + 1, result["x"] = imax < 4 ? xmax - ledPositions[imax] : -1;
    return result;
  }
  bool wagonRoom(unsigned int room, unsigned loop)
  {
    std::vector < stepsolver::Numeric > positions = {
      stepsolver::Numeric("{zero:0 min:0 max:50000 precision:5}")
    };
    class WagonDriver: public stepsolver::Driver {
      mutable double positions[1] = { 0 };
      const double *get() const
      {
        static const double roomWidth = 2000;
        wjson::Value where = wagonWhere();
        positions[0] = where.get("room", 0) + where.get("x", 0) / roomWidth;
        return positions;
      }
      void set(const double values[])
      {
        double dx = values[0] - positions[0];
        wagonShift(dx);
      }
    }
    wagonDriver;
    stepsolver::Controller controller(positions, wagonDriver);
    double values[1] = { (double) room };
    return controller.iterate(values, loop);
  }
}
