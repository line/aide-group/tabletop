#
# This shows the diffferent commands to install a wifi access point
#


# 1/ Generates the configuration files

cat > /etc/dhcpcd.conf.access_point.trailer <<EOF

# Wifi Access Point static IP configuration

interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
EOF
cat /etc/dhcpcd.conf.orig /etc/dhcpcd.conf.access_point.trailer > /etc/dhcpcd.conf

cat > /etc/dnsmasq.conf <<EOF

# Wifi Access Point DHCP range configuration

interface=wlan0
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
EOF

cat > /etc/hostapd/hostapd.conf.access_point <<EOF

# Wifi Access Point hostapd deamon configuration

interface=wlan0
driver=nl80211
ssid=aide_tabletop
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=aide,moi,tabletop,t,aidera
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
EOF

cat > /etc/default/hostapd <<EOF

# Wifi Access Point hostapd deamon configuration reference

DAEMON_CONF="/etc/hostapd/hostapd.conf.access_point"
EOF

# 2/ Starts the service

systemctl start dnsmasq
systemctl unmask hostapd
systemctl start hostapd
systemctl enable dnsmasq
systemctl enable hostapd
sudo reboot
