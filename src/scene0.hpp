#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>

namespace tabletop {
  // Compares a sample image against prototypes image using a user defined similarity() function
  class PrototypesComparer {
    wjson::Value results, doSimilarity;
public:
    // Constructs the comparer considering the predefined parameters
    void construct(unsigned int room, String name, bool balance)
    {
      JSON what = getParameters()["rooms"][room - 1][name],
           prototypes = what.at("prototypes"),
           area = what.at("area"),
           doSimilarity = what.at("similarity");
      String base = getParameters()["rooms"][room - 1].get("base", "");
      construct(base, prototypes, area, doSimilarity, balance);
    }
    void construct(String base, JSON prototypes, JSON area, JSON doSimilarity, bool balance)
    {
      this->doSimilarity = doSimilarity;
      for(unsigned int k = 0; k < prototypes.length(); k++) {
        // Prepares the result structure
        String name = prototypes.get(k, "");
        results[k]["k"] = k;
        results[k]["name"] = name;
        // Loads the prototypes in cache
        aidecv::Image prototype(base + name + ".png");
        if(balance) {
          prototype.set("{do: balance saturation: 5}");
        }
        prototype.set(aidesys::echo("{do: resize width: %d height: %d}", area.get("width", 0), area.get("height", 0)));
        prototype.set("{do: cache what: put name: " + name + "}");
      }
    }
private:
    // Allows to to sort the results in decreasing order
    static bool before(JSON lhs, JSON rhs)
    {
      return lhs.get("c", 0.0) < rhs.get("c", 0.0) || (lhs.get("c", 0.0) == rhs.get("c", 0.0) && lhs.get("k", 0.0) < rhs.get("k", 0.0));
    }
public:
    // Gets the comparison for a given sample
    JSON get(aidecv::Image& sample)
    {
      static wjson::Value result;
      result.clear();
      // Calculates the similarity against all prototypes
      for(unsigned int k = 0; k < results.length(); k++) {
        aidecv::Image prototype("{do: cache what: get name: '" + results[k].get("name", "") + "'}");
        sample.set(doSimilarity, prototype);
        results[k]["c"] = sample.parameters.at("similarity").get("value", 0.0);
        result[result.length()] = results[k];
      }
      // Sorts in decreasing order and return
      result.sort(before);
      return result;
    }
  };
}
