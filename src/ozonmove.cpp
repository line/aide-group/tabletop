#include "ozon.hpp"
#include "Controller.hpp"
#include "Image.hpp"
#include "time.hpp"

namespace tabletop {
  // Returns the ozon position (i, j) in pixels and orientation a in degree
  JSON ozonWhere()
  {
    // Gets the ozon LEDs views
    static const unsigned int device = 4;
    static const char color = 'y';
    // Grabs a 1st view with background suppression
    ozonLight(0xFF, '0');
    aidecv::Image grabr0(device);
    ozonLight(0x01, color);
    aidecv::Image grab0(device);
    ozonLight(0xFF, '0');
    ozonForward(5);
    grab0.set("{do: subtract threshold: 50}", grabr0);
    grab0.set("{do: blob blue: 170 green: 128 red: 0 hue_threshold: 60 value_threshold: 0.15 }");
    // Translates a little to get orientation
    aidesys::sleep(1000);
    aidecv::Image grabr1(device);
    ozonLight(0x01, color);
    aidecv::Image grab1(device);
    ozonLight(0xFF, '0');
    grab1.set("{do: subtract threshold: 50}", grabr1);
    grab1.set("{do: blob blue: 170 green: 128 red: 0 hue_threshold: 60 value_threshold: 0.15 }");
#if 0
    // Saves for debug
    {
      grab0.set("{do: save path: upload/ozonmove/grab0_done}");
      grab1.set("{do: save path: upload/ozonmove/grab1_done}");
    }
#endif
    // Reports result
    static wjson::Value result;
    result.clear();
    JSON blob0 = grab0.parameters.at("blob"), blob1 = grab1.parameters.at("blob");
    if(blob0.get("size", 0) > 0 && blob1.get("size", 0) > 0) {
      double x0 = blob0.get("x", 0.0), x1 = blob1.get("x", 0.0), dx = x1 - x0;
      double y0 = blob0.get("y", 0.0), y1 = blob1.get("y", 0.0), dy = y1 - y0;
      result["x"] = rint(x1);
      result["y"] = rint(y1);
      result["d"] = rint(sqrt(dx * dx + dy * dy));
      result["r"] = rint(sqrt(blob0.get("size", 0.0) / M_PI));
      result["a"] = rint(180.0 / M_PI * atan2(dy, dx));
    } else {
      result["x"] = result["y"] = -1, result["d"] = result["r"] = result["a"] = 0;
    }
    return result;
  }
  void ozonMove(unsigned int x, unsigned int y, int a, unsigned int loop)
  {
    std::vector < stepsolver::Numeric > positions = {
      stepsolver::Numeric("{zero:0 min:0 max:4000 precision:5}"),
      stepsolver::Numeric("{zero:0 min:0 max:4000 precision:5}"),
      stepsolver::Numeric("{zero:0 min:-180 max:360 precision:10}")
    };
    class OzonDriver: public stepsolver::Driver {
      mutable double positions[3] = { 0, 0, 0 };
public:
      const double *get() const
      {
        wjson::Value where = ozonWhere();
        positions[0] = where.get("x", 0.0);
        positions[1] = where.get("y", 0.0);
        positions[2] = where.get("a", 0.0);
        return positions;
      }
      void set(const double values[])
      {
        moveOnce(values, false);
      }
      void moveOnce(const double values[], bool final = false)
      {
        double dx = values[0] - positions[0], dy = values[1] - positions[1], dl = 0.5 * sqrt(dx * dx + dy * dy);
        double da = 180 / M_PI * atan2(dy, dx), da0 = da - positions[2], da1 = values[2] - da;
        da0 = da0 > 180 ? da0 - 360 : da0 < -180 ? da0 + 360 : da0;
        da1 = da1 > 180 ? da1 - 360 : da1 < -180 ? da1 + 360 : da1;
        if(!final) {
          ozonTurn(-rint(da0));
          ozonForward(rint(dl));
        } else {
          ozonTurn(-rint(da1));
        }
      }
    }
    ozonDriver;
    if(loop == 1) {
      double values[3] = { (double) x, (double) y, (double) a };
      ozonDriver.get();
      ozonDriver.moveOnce(values, false);
      ozonDriver.moveOnce(values, true);
    } else {
      stepsolver::Controller controller(positions, ozonDriver);
      double values1[3] = { (double) x, (double) y, (double) a };
      controller.iterate(values1, loop);
      double values2[3] = { (double) x, (double) y, (double) a };
      ozonDriver.moveOnce(values2, true);
    }
  }
}
