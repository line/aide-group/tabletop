#include "tabletop.hpp"
#include "std.hpp"
#include "time.hpp"
#include "file.hpp"
#include "Image.hpp"

namespace tabletop {
  std::string command(const char *command)
  {
    static std::string usage = (std::string)
                               "Usage:\n" +
                               "  c[1234][+-]    to open a room cover\n" +
                               "  d[1234][+-]    to open a room door\n" +
                               "  g[ab]          to put the gate in a or b position\n" +
                               "  o[<>]          to turn ozon left or right\n" +
                               "  of$d           to move ozon forward distance in mm\n" +
                               "  ol[w0rgbycm]$i to set a ([w]hite, [0]black, [r]ed, [g]reen, [b]lue, [y]ellow, [c]yan, [m]agenta) color for the given led indexes $i in hexadeximal\n" +
                               "  o?             to get ozon position\n" +
                               "  o$i,$j,$a      to move ozon to the ($i,$j) pixel camera location with $a orientation in degree\n" +
                               "  w[<>]$d        to shift the wagon left or right, a given distance $d in pixel\n" +
                               "  w[1234]        to position the wagon in room 1,2,3 or 4\n" +
                               "  i              to grab an image and save it in a file, without preprocessing\n" +
                               "  i0             to process the current image and save it in a file\n" +
                               "  i[1234]s       to correct the shift of a given room\n" +
                               "  i[1234]p       to parse the scene of a given room\n" +
                               "  i[1234]d       to detect the next stable view and parse the scene of a given room\n" +
                               "  h              to get this help\n";
    std::string answer = aidesys::echo("unknownCommand(%s)\n", command);
    switch(command[0]) {
    case 'h':
      return usage;
    case 'c':
    {
      int r;
      if(sscanf(&command[1], "%d", &r) == 1) {
        switch(command[2]) {
        case '+':
        case '-':
          coverSet(r, command[2] == '+');
          answer = aidesys::echo("coverSet(%d, '%c')\n", r, command[2]);
          break;
        }
      }
      break;
    }
    case 'd':
    {
      int r;
      if(sscanf(&command[1], "%d", &r) == 1) {
        switch(command[2]) {
        case '+':
        case '-':
          doorSet(r, command[2] == '+');
          answer = aidesys::echo("doorSet(%d, '%c')\n", r, command[2]);
          break;
        }
      }
      break;
    }
    case 'g':
    {
      switch(command[1]) {
      case 'a':
      case 'b':
        gateSet(command[1] == 'a');
        answer = aidesys::echo("gateSet('%c')\n", command[1]);
        break;
      }
      break;
    }
    case 'o':
    {
      switch(command[1]) {
      case '<':
      case '>':
      {
        ozonTurn(command[1] == '<' ? 90 : -90);
        answer = aidesys::echo("ozonTurn('%c')\n", command[1]);
        break;
      }
      case 'f':
      {
        int d;
        if(sscanf(&command[2], "%d", &d) == 1) {
          ozonForward(d);
          answer = aidesys::echo("ozonForward(%d)\n", d);
        }
        break;
      }
      case 'l':
      {
        int i;
        if(sscanf(&command[3], "%x", &i) == 1) {
          ozonLight(i, command[2]);
          answer = aidesys::echo("ozonLight(0x%x, '%c')\n", i, command[2]);
        }
        break;
      }
      case '?':
      {
        answer = aidesys::echo("ozonWhere(%s)\n", ozonWhere().asString(false, true).c_str());
        break;
      }
      default:
      {
        int x, y, a;
        if(sscanf(&command[1], "%d,%d,%d", &x, &y, &a) == 3) {
          ozonMove(x, y, a, 1);
          answer = aidesys::echo("ozonMove(%d, %d, %d)\n", x, y, a);
        }
      }
      }
      break;
    }
    case 'w':
    {
      switch(command[1]) {
      case '<':
      case '>':
      {
        int distance;
        if(sscanf(&command[2], "%d", &distance) == 1) {
          wagonShift(command[1] == '<' ? -distance : distance);
          answer = aidesys::echo("wagonShift('%c', %d)\n", command[1], distance);
        }
        break;
      }
      case '1':
      case '2':
      case '3':
      case '4':
      {
        int room;
        if(sscanf(&command[1], "%1d", &room) == 1) {
          wagonRoom(room);
          answer = aidesys::echo("wagonRoom(%d)\n", room);
        }
      }
      }
      break;
    }
    case 'i':
    {
      std::string name = "./upload/pictures/grab-" + aidesys::nowISO();
      aidesys::system("mkdir -p ./upload/pictures/");
      switch(strlen(command)) {
      case 1:
      {
#ifdef ON_RASPPI
        static const unsigned int device = 4;
#else
        static const unsigned int device = 0;
#endif
        aidecv::Image grab(device);
        grab.set("{do: save path: '" + name + "'}");
        answer = aidesys::echo("getGrab('%s')\n", name.c_str());
        break;
      }
      case 2:
      {
        if(command[1] == '0') {
          aidecv::Image& view = getView();
          view.set("{do: save path: '" + name + "'}");
          answer = aidesys::echo("getView('%s')\n", name.c_str());
        }
        break;
      }
      case 3:
      {
        unsigned int room = 0;
        if(sscanf(&command[1], "%1d", &room) == 1 && 1 <= room && room <= 4) {
          switch(command[2]) {
          case 's':
          {
            aidecv::Image& shift = setViewShift(room);
            shift.set("{do: save path: '" + name + "'}");
            answer = aidesys::echo("getShift(%d, '%s')\n", room, name.c_str());
            break;
          }
          case 'p':
          {
            aidecv::Image& scene = getScene(room);
            scene.set("{do: save path: '" + name + "'}");
            answer = aidesys::echo("getScene(%d, '%s')\n", room, name.c_str());
            break;
          }
          case 'd':
          {
            aidecv::Image& scene = getScene(room, true);
            scene.set("{do: save path: '" + name + "'}");
            answer = aidesys::echo("getScene(%d, '%s')\n", room, name.c_str());
            break;
          }
          }
        }
        break;
        break;
      }
      }
    }
    }
    return answer;
  }
}
