#!/bin/bash

# Detects if in production or development mode and set target accordingly

roof="`pwd`" ; roof="`dirname $roof`" ; roof="`dirname $roof`" ; roof="`basename $roof`"

if [ "$roof" = "node_modules" ]
then where=../../../public/
else where=../public/
fi

# Gets the last version of webcontrols elements

mkdir -p $where
rm -f $where/webcontrols.*
wget -q https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/webcontrols.js -P $where
wget -q https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/webcontrols.css -P $where
