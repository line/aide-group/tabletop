#include "ozon.hpp"
#include "parameters.hpp"
#include "python.hpp"

// Used for the Python wrapper
#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace tabletop {
  // Ozon command init flag
  bool ozonCommandInit = true;

  void ozonCommand(String command)
  {
#ifndef ON_RASPPI
    aidesys::alert("  illegal-state", "in tabletop::ozonCommand, not on a RaspPi");
#else
    if(ozonCommandInit) {
      aidesys::python("start");
      printf("Connection to ozon, this takes a few seconds ...\n");
      int err = PyRun_SimpleString("from ozoevo.bt import RobotFinder\n"
                                   "from ozoevo.robot import OzoEvo\n"
                                   "finder = RobotFinder()\n"
                                   "finder.find_robots()\n"
                                   "finder\n"
                                   "ozon = OzoEvo(finder['AideLine'].mac_addr)\n"
                                   "ozon.connect()\n");
      printf(".. connection done\n");
      aidesys::alert(err != 0, "illegal-state", "in tabletop::ozonCommand, unable to start the command, is the ozobot on ?");
      ozonCommandInit = false;
    }
    // -printf("ozonCommand(%s)\n", command.c_str());
    int err = PyRun_SimpleString(command.c_str());
    aidesys::alert(err != 0, "  illegal-state", "in tabletop::ozonCommand, unable to run the command '" + command + "'");
#endif
  }
  void ozonLight(unsigned int index, unsigned char blue, unsigned char green, unsigned char red)
  {
    ozonCommand(aidesys::echo("ozon._send_led_control_command(%d, %d, %d, %d)", index, red, green, blue));
  }
  void ozonLight(unsigned int index, char c)
  {
    switch(c) {
    case 'w':
      ozonLight(index, 255, 255, 255);
      break;
    case 'r':
      ozonLight(index, 0, 0, 255);
      break;
    case 'g':
      ozonLight(index, 0, 255, 0);
      break;
    case 'b':
      ozonLight(index, 255, 0, 0);
      break;
    case 'y':
      ozonLight(index, 255, 255, 0);
      break;
    case 'c':
      ozonLight(index, 0, 255, 255);
      break;
    case 'm':
      ozonLight(index, 255, 0, 255);
      break;
    case '0':
    default:
      ozonLight(index, 0, 0, 0);
      break;
    }
  }
  void ozonForward(unsigned int distance)
  {
    unsigned int forwardvelocity = getParameters()["ozon"].get("forwardvelocity", 0);
    double forward2duration = getParameters()["ozon"].get("forward2duration", 0.0);
    ozonCommand(aidesys::echo("ozon.drive(%d, %d, %f, True)", forwardvelocity, forwardvelocity,
                              0.001 * distance * forward2duration));
  }
  void ozonTurn(int angle)
  {
    unsigned int turnvelocity = getParameters()["ozon"].get("turnvelocity", 0);
    double turn2duration = getParameters()["ozon"].get("turn2duration", 0.0);
    ozonCommand(aidesys::echo("ozon.spin(%d, %f, True)", angle > 0 ? -turnvelocity : turnvelocity,
                              0.001 * fabs(angle) * turn2duration));
  }
  void ozonStop()
  {
    ozonCommand("ozon._send_led_control_command(255, 0, 0, 0)");
    ozonCommand("ozon.drive(0, 0, 0.001, False)");
    ozonCommand("ozon.spin(0, 0.001, False)");
  }
}
