namespace tabletop {
  class ButtonParser {
    PrototypesComparer comparer;
    wjson::Value button_area;
    void construct()
    {
      if(!button_area.isRecord()) {
        wjson::Value& button_area = getParameters()["rooms"][0]["button"]["area"];
        button_area["width"] = button_area.get("right", 0) - button_area.get("left", 0);
        button_area["height"] = button_area.get("bottom", 0) - button_area.get("top", 0);
        this->button_area = button_area;
        comparer.construct(1, "button", false);
      }
    }
public:
    void parse(aidecv::Image& view)
    {
      construct();
      view.set("{do: draw what:[" + button_area.asString() + "]}");
      aidecv::Image area(button_area, view);
      JSON result = comparer.get(area);
      view.parameters["parse"]["button"] = result;
      view.set(aidesys::echo("{do: draw what:[{do: text x: %d y: %d text: \"" + result.at(0).get("name", "") + "\" color:yellow}]}",
                             (button_area.get("left", 0) + button_area.get("right", 0)) / 2, (button_area.get("top", 0) + button_area.get("bottom", 0)) / 2));
    }
  }
  buttonParser;

  class BlocksParser {
    PrototypesComparer comparer_fblocks, comparer_tblocks;
    wjson::Value block_areas;
    unsigned int fthreshold = 0;
    void construct()
    {
      if(!block_areas.isRecord()) {
        wjson::Value& fblocks_area = getParameters()["rooms"][0]["fblocks"]["area"];
        fthreshold = getParameters()["rooms"][0]["fblocks"].get("fthreshold", 0);
        wjson::Value& tblocks_area = getParameters()["rooms"][0]["tblocks"]["area"];
        fblocks_area["width"] = tblocks_area["width"] = fblocks_area.get("right", 0) - fblocks_area.get("left", 0);
        fblocks_area["height"] = tblocks_area["height"] = rint((fblocks_area.get("bottom", 0) - fblocks_area.get("top", 0)) / 8.0);
        for(unsigned int j = 0; j < 8; j++) {
          block_areas[j] = fblocks_area;
          block_areas[j]["color"] = "red";
          block_areas[j]["top"] = fblocks_area.get("top", 0) + j * fblocks_area.get("height", 0);
          block_areas[j]["bottom"] = fblocks_area.get("top", 0) + (j + 1) * fblocks_area.get("height", 0);
        }
        comparer_fblocks.construct(1, "fblocks", false);
        comparer_tblocks.construct(1, "tblocks", false);
      }
    }
public:
    void parse(aidecv::Image& view)
    {
      construct();
      for(unsigned int j = 0; j < 8; j++) {
        JSON block_area = block_areas[j];
        view.set("{do: draw what:[" + block_area.asString() + "]}");
        aidecv::Image area(block_area, view);
        JSON fresult = comparer_fblocks.get(area);
        if(fresult.at(1).get("c", 0.0) - fresult.at(0).get("c", 0.0) < fthreshold) {
          JSON tresult = comparer_tblocks.get(area);
          view.parameters["parse"]["blocks"][j] = tresult;
        } else {
          view.parameters["parse"]["blocks"][j] = fresult;
        }
        view.set(aidesys::echo("{do: draw what:[{do: text x: %d y: %d text: \"" + view.parameters.at("parse").at("blocks").at(j).at(0).get("name", "") + "\" color:yellow}]}",
                               (block_area.get("left", 0) + block_area.get("right", 0)) / 2, (block_area.get("top", 0) + block_area.get("bottom", 0)) / 2));
      }
    }
  }
  blocksParser;
}
