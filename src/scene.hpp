#ifndef __tabletop_scene__
#define __tabletop_scene__

#include "Value.hpp"
#include "Image.hpp"

namespace tabletop {
  /**
   * @class scene
   * @description Specifies the visual scene analysis high-level functions.
   * - These functions are accessible via the `tabletop::` prefix.
   */

  /**
   * @function getView
   * @memberof scene
   * @static
   * @description Grabs an image from the raspicam.
   *  - Applies white-balance on the image
   *  - Rectifies the image given the predefined parameters and horizontal shifts.
   * @param {bool} [with_spot= false]
   * - If true, waits untill something has been changed in the scene.
   * - If false, returns immediatly.
   * @return A reference to the grabbed image, with the parsed information in the "room" section of the image meta-data.
   */
  aidecv::Image& getView(bool with_spot = false);

  /**
   * @function setViewShift
   * @memberof scene
   * @static
   * @description Adjusts the horizontal shift, between the reference image and the present view.
   * @param {unit} room The room number 1,2,3 or 4.
   *  - The room reference image and rectangular zone to take intoaccount is defined in the `parameters.json` file.
   * @return A reference to the grabbed image cropped zone, with the processing information.
   */

  aidecv::Image& setViewShift(unsigned int room);

  /**
   *
   * @function getScene
   * @memberof scene
   * @static
   * @description Grabs and parses an image from the raspicam.
   * @param {uint} [room=0] Parses the image considering being in the room of number 1 to 4.
   * @param {bool} [with_event_detection= false]
   * - If true, waits untill something has been changed in the scene.
   * - If false, returns immediatly.
   * @return A reference to the grabbed image, with the parsed information in the "room" section of the image meta-data.
   */
  aidecv::Image& getScene(unsigned int room = 0, bool with_event_detection = false);
}
#endif
