const express = require("express");
const server = express();

// Used to test if the server is alive 

server.get("/", function(request, response) {
  response.send("<h2>Tabletop server is alive: <center>[<a href='play/index.html'>play</a>] [<a href='public/controlPanel.html'>control</a>]  [<a href='public/ozonPanel.html'>ozon</a>]</center></h2>\n");
});

// Displays the demo pages

server.use("/play", express.static("play"));
server.use("/public", express.static("public"));
server.use("/upload", express.static("upload"));

// Defines the web services

const PersistantDataService = require("../node_modules/aideweb/public/PersistantDataService.js");
new PersistantDataService(server, "/upload");
const WrapperCCService = require("../node_modules/aideweb/public/WrapperCCService.js");
new WrapperCCService(server);
const ServerEventService = require("../node_modules/aideweb/public/ServerEventService.js");
new ServerEventService(server);
const StopService = require("../node_modules/aideweb/public/StopService.js");
new StopService(server);

// Starts the service

console.log("Starting the tabletop service ...");
server.listen(8080);
