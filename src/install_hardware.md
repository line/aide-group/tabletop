# Concrete elements to reproduce or derive the setup hardware

The hardware methodology and realization is described in the [open hardware section of the reference paper](https://docs.google.com/document/d/1CPCAh4e8rkpbsYcHazhRU_V3ZmpntwDhkCtOPnSbQhU/edit#heading=h.ozgzsgafpzxd), the present documentation provides concrete elements to reproduce the hardware or derive another setup. Please do not hesitate to contact us for further information and help.

## Mechanical realization

The present mechanical realization is made of standard woodworking, using salvage wood, it is nothing more than a crate with a double bottom, the electronic being in the cellar and the game play space above. 
Regarding the object animation, low-cost electronics has been considered, with standard angular and linear servo-motors and a low cost ESP32 controller, as sketched out here:

![general hardware setup](./www/img/about-hardware-1.png)

- A schematic description of the hardware design choices, using low-cost easily reusable hardware components,

A step further:

- The hardware plans are available here with all dimensions: [woodwork plans](https://drive.google.com/drive/folders/1e9cGL5O9CscnfVxPBrGF2aq-AxWJZbzW)
- The contact paper plans are all available here [contact paper plans](https://drive.google.com/drive/folders/16SHd986AVKULfyCbZjNf3VVmhbzxgOsW) in ready to print format.
- Some [photos and videos](https://drive.google.com/drive/folders/1-7fNC8Tg1OebYuPviDwkSbMvX5ySGRCq) of the realization (under the same CC-BY license as all other resources).

![hardware photo gallery](./www/img/about-hardware-2.png)

- A: The Raspberry computer tablet computer mounted on a carriage with the image analysis camera Raspicam mounted on a pole to provide a top view of the game-play space.
- B: A view of the work site, showing that everything has been built with standard craft tools.
- C and D: Detailed views of an automatic gate between two rooms, with the controller stick in the cellar part of the box.
- E and F: Detailed views of the locker mechanism, the bolt fastener has been produced on a 3D printer in a maker-space.
- G: A view of the game board before putting it in place, all decoration is printed on a contact paper stuck on a thin wood plank. 
- H and I: Detailed view of the wagon mechanism allowing the tablet to move along the game sequence.
- J: A view of the setup when closed, allowing to easily move it from one place to another.

## Electronic realization

The complete list of components is given here with precise links towards an example of (non exclusive) provider, please consider this _only_ as an illustrative example, to help redesigning the hardware. The present electronic component total cost order of magnitude is of about 600€ which can easily be reduced to more than half by considering manual manipulation of the wagon and manual bolts with padlock.

### Main computing device

1. A low-cost [RPi 3B+](https://thepihut.com/collections/raspberry-pi/products/raspberry-pi-3-model-b-plus) with a recommended [heat sink](https://www.mouser.fr/ProductDetail/Seeed-Studio/114991561?qs=sGAEpiMZZMtyU1cDF2RqUPJ2zRX2gxWEOYokptCe6gA=)
2. A [Raspberry Pi Camera Module](https://thepihut.com/collections/raspberry-pi-camera/products/raspberry-pi-camera-module) with its [flex cable](https://thepihut.com/products/flex-cable-for-raspberry-pi-camera-or-display-2-meters)
3. A [PWM servo bonnet card](https://www.mouser.fr/ProductDetail/Adafruit/3416?qs=/ha2pyFadug6CDpjN0WmZzOkOqAjrMTchcKxKfh0yvg=) used to drive the wagon from the RPi card
4. A standard additional webcam, any low cost web cam suitable.
5. A standard 32Go fast (at least 10MB/s) SD-card to copy the [Raspbian SD card image](https://files.inria.fr/mecsci/aide/Raspbian-AIDE.img.gz), as detailed in the [RPi installation](./install_rasppi.md).
6. The [RasPad tablet](https://www.sunfounder.com/raspberrypi-tablet-raspad.html) with touchscreen, loudspeaker, microphone and power supply, mounted on a [rotating support](https://www.amazon.fr/gp/product/B00D9MVEM4/ref=ox_sc_act_title_1?smid=A1X6FK5RDHNB96&psc=1).

Cost order of magnitude: 100€ for items 1 to 5 + 230€ for item 6. 

Using the RasPad saved a lot of time and allowed a rather nice final look, but there is a cheaper solution using a [SmartiPi TouchPad](https://www.adafruit.com/product/3187) and acquiring the complementary components (microphone, loudspeaker, power supply), cost could belower by a factor of 2 to 3, with the burden to design the case.

### The Wagon specific hardware

1. Two [wheels](https://www.mouser.fr/ProductDetail/Digilent/240-040?qs=sGAEpiMZZMvPl9QmpP6ZCTeFijLnkLxnnI25JplDSxpWmWsX1FCihA==) that have been mechanically adapted using salvaged plumbing seals.
2. Two [continuous servo motor](https://www.mouser.fr/ProductDetail/Adafruit/2442?qs=%2Fha2pyFadugnuhGeiIIZTSdAbCRbw0QRRp%252B17vDtvldrcuaGwT56a12j5JBhIKFy)
3. A [sliding rail](https://www.ebay.fr/itm/SBR12-1000-2000mm-Lineaire-Rail-de-Guidage-Guide-de-Glissiere-4x-SBR12UU-Bloc-/254647214730)

![sliding rail](./www/img/sliding-rail.png)

Cost order of magnitude: 20€ for items 1 and 2 + 40€ for item 3. 

### Tabletop servo motors

1. 6 [micro angular servo motors](https://www.mouser.fr/ProductDetail/dfrobot/ser0006/?qs=kE1vTINknaVeJIz3lDBUDw==)
2. 8 [linear servo motors](https://www.gotronic.fr/art-servomoteur-lineaire-vs19-26174.htm), which are very fragile, they can not be assembled with strong glue, unless steam is avoided.

Cost order of magnitude: 3€ for angular servo each, and 10€ for linear one.

### ESP32 devices

Each (there are two in the present setup) ESP32 require:

1. [Adafruit HUZZAH32 ESP32 Feather Board](https://www.mouser.fr/ProductDetail/Adafruit/3619?qs=sGAEpiMZZMuJ3l9lTgMBp1ZUJQFPkBG2f%2FoxhGRpoYLhePjBLmftBw%3D%3D)
2. [PWM or servo featherwing add-on](https://www.mouser.fr/ProductDetail/485-2928) and [related connectors](https://www.mouser.fr/ProductDetail/485-283)
3. A micro-B USB connector for salvage with this [micro-usb-wiring](img/micro-usb-wiring.png).

Cost order of magnitude: 30€ each.

### Power Supply

Two power supplies for the wagon and the tabletop itself, any 5volt 1 ampere (about 0.5A by servomotor without charge, 1A by motor for the wagon) power supply is suitable.

Cost order of magnitude: 20 to 30€ each.
